package ngrams;

/**
 * Write a description of class MarkovZero here.
 * 
 * @author Duke Software
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Random;

import ngrams.interfaces.IMarkovModel;

public class MarkovModel implements IMarkovModel {
	private String myText;
	private Random myRandom;
	private int predict;

	public MarkovModel(int predict) {
		myRandom = new Random();
		this.predict = predict;
	}

	public void setRandom(int seed) {
		myRandom = new Random(seed);
	}

	public void setTraining(String s) {
		myText = s.trim();
	}

	public String getRandomText(int numChars) {
		if (myText == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		int index = myRandom.nextInt(myText.length() - predict);
		String key = myText.substring(index, index + predict);
		sb.append(key);
		for (int k = 0; k < numChars - key.length(); k++) {
			ArrayList<String> follows = getFollows(key);
			if (follows.size() == 0) {
				break;
			}
			index = myRandom.nextInt(follows.size());
			String next = follows.get(index);
			sb.append(next);
			key = next;
		}
		return sb.toString();
	}

	public ArrayList<String> getFollows(String key) {
		ArrayList<String> res = new ArrayList<>();
		int idx = 0;
		while (idx < myText.length() - 3 && idx != -1) {
			idx = myText.indexOf(key, idx);
			if (idx + key.length() + 1 <= myText.length() && idx != -1) {
				res.add(myText.substring(idx + key.length(), idx + key.length() + 1));
				idx += key.length();
			} else {
				idx = myText.length() - 3;
				break;
			}
		}
		return res;
	}

}
