package ngrams.interfaces;

/**
 * Write a description of class MarkovRunner here.
 * 
 * @author Duke Software
 * @version 1.0
 */

import edu.duke.*;
import ngrams.Tester;

public class MarkovRunnerWithInterface {

	public static void main(String[] argv) {
		MarkovRunnerWithInterface mr = new MarkovRunnerWithInterface();
		mr.runMarkov();
	}

	public void runModel(IMarkovModel markov, String text, int size, int seed) {
		markov.setTraining(text);
		markov.setRandom(seed);
		System.out.println("running with " + markov);
		for (int k = 0; k < 3; k++) {
			String st = markov.getRandomText(size);
			printOut(st);
		}
	}

	public void runMarkov() {
		FileResource fr = new FileResource(//
				Tester.path + "confucius.txt");
		String st = fr.asString();
		st = st.replace('\n', ' ');
		int size = 200;
		int seed = 531;

		// MarkovZero mz = new MarkovZero();
		// runModel(mz, st, size, seed);
		//
		// MarkovOne mOne = new MarkovOne();
		// runModel(mOne, st, size, seed);
		//
		// MarkovModel mThree = new MarkovModel(3);
		// runModel(mThree, st, size, seed);
		//
		// MarkovFour mFour = new MarkovFour();
		// runModel(mFour, st, size, seed);

		EfficientMarkovModel em = new EfficientMarkovModel(5);
		runModel(em, st, size, seed);

		
	}

	public void testHashMap() {
		FileResource fr = new FileResource(//
				Tester.path + "confucius.txt");
		String st = fr.asString();
		st = st.replace('\n', ' ');
		st = "yes-this-is-a-thin-pretty-pink-thistle";
		int size = 50;
		int seed = 42;
		EfficientMarkovModel em = new EfficientMarkovModel(3);
		runModel(em, st, size, seed);

	}

	public void compareMethods() {
		FileResource fr = new FileResource(//
				Tester.path + "hawthorne.txt");
		String st = fr.asString();
		st = st.replace('\n', ' ');
		int size = 1000;
		int seed = 42;

		EfficientMarkovModel em = new EfficientMarkovModel(2);
		runModel(em, st, size, seed);

		MarkovModel mThree = new MarkovModel(2);
		runModel(mThree, st, size, seed);

	}

	private void printOut(String s) {
		String[] words = s.split("\\s+");
		int psize = 0;
		System.out.println("----------------------------------");
		for (int k = 0; k < words.length; k++) {
			System.out.print(words[k] + " ");
			psize += words[k].length() + 1;
			if (psize > 60) {
				System.out.println();
				psize = 0;
			}
		}
		System.out.println("\n----------------------------------");
	}

}
