package ngrams.interfaces;

/**
 * Write a description of class MarkovZero here.
 * 
 * @author Duke Software
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Random;

public class MarkovModel extends AbstractMarkovModel {
	private int order;

	public MarkovModel(int n) {
		myRandom = new Random();
		this.order = n;
	}

	public void setRandom(int seed) {
		myRandom = new Random(seed);
	}

	public void setTraining(String s) {
		myText = s.trim();
	}

	public String getRandomText(int numChars) {
		if (myText == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		int index = myRandom.nextInt(myText.length()-(order+1));
		String key = myText.substring(index, index+order);
		sb.append(key);

		for(int k=0; k < numChars-order; k++){
			ArrayList<String> follows = getFollows(key);
			//System.out.println("key "+key+" "+follows);
			if (follows.size() == 0) {
				break;
			}
			index = myRandom.nextInt(follows.size());
			String next = follows.get(index);
			sb.append(next);
			key = key.substring(1) + next;
		}

		return sb.toString();
	}

	@Override
	public String toString() {
		return "MarkovModel of order " + order;
	}
}
