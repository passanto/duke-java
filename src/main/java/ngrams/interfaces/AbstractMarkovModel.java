package ngrams.interfaces;

/**
 * Abstract class AbstractMarkovModel - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */

import java.util.*;

public abstract class AbstractMarkovModel implements IMarkovModel {
	protected String myText;
	protected Random myRandom;
	protected int order;

	public AbstractMarkovModel() {
		myRandom = new Random();
	}

	public AbstractMarkovModel(int order) {
		myRandom = new Random();
		this.order = order;
	}
	public void setTraining(String s) {
		myText = s.trim();
	}

	public void setRandom(int seed) {
		myRandom = new Random(seed);
	}

	abstract public String getRandomText(int numChars);

	protected ArrayList<String> getFollows(String key) {
		ArrayList<String> res = new ArrayList<>();
		int idx = 0;
		while (idx < myText.length() - 3 && idx != -1) {
			idx = myText.indexOf(key, idx);
			if (idx + key.length() + 1 <= myText.length() && idx != -1) {
				res.add(myText.substring(idx + key.length(), idx + key.length() + 1));
				idx += key.length();
			} else {
				idx = myText.length() - 3;
				break;
			}
		}
		return res;
	}

}
