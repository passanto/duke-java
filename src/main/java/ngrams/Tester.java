package ngrams;

import edu.duke.FileResource;

public class Tester {

	public static String path = "/home/antonio/eclipse-workspace/JavaProgramminCourse/src/main/resources/ngrams/";

	public static void main(String[] argv) {
		Tester t = new Tester();
		t.testGetFollowsWithFile();
	}

	public void testGetFollows() {
		MarkovOne m = new MarkovOne();
		m.setTraining("this is a test yes this is a test.");
		System.out.println(m.getFollows("t"));
	}

	public void testGetFollowsWithFile() {
		MarkovOne m = new MarkovOne();
		String st = new FileResource(path + "confucius.txt").asString().replace('\n', ' ');
		m.setTraining(st);
		System.out.print("res:" + m.getFollows("he").size());
	}

}
