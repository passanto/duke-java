package ngrams;

/**
 * Write a description of class MarkovWordOne here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import ngrams.interfaces.IMarkovModel;

import java.util.ArrayList;
import java.util.Random;

public class MarkovWordTwo implements IMarkovModel {
    private String[] myText;
    private Random myRandom;

    public MarkovWordTwo() {
        myRandom = new Random();
    }
    
    public void setRandom(int seed) {
        myRandom = new Random(seed);
    }
    
    public void setTraining(String text){
		myText = text.split("\\s+");
	}
	
	public String getRandomText(int numWords){
		StringBuilder sb = new StringBuilder();
		int index = myRandom.nextInt(myText.length-2);  // random word to start with
		String key1 = myText[index];
		String key2 = myText[index+1];
		sb.append(key1).append(" ").append(key2).append(" ");
		for(int k=0; k < numWords-1; k++){
		    ArrayList<String> follows = getFollows(key1, key2);
		    if (follows.size() == 0) {
		        break;
		    }
			index = myRandom.nextInt(follows.size());
			String next = follows.get(index);
			sb.append(next).append(" ");
			key1 = key2;
			key2 = next;
		}
		
		return sb.toString().trim();
	}


	private ArrayList<String> getFollows(String key1, String key2) {
		ArrayList<String> follows = new ArrayList<>();
		int pos = 0;
		while(pos < myText.length){
			int start = indexOf(myText, key1, key2, pos);
			if(start == -1 || start + 1 >= myText.length){break;}
			String next = myText[start+1];
			follows.add(next);
			pos = start+1;
		}
		return follows;
	}

	public void testIndexOf() {
		String[] words = "this is just a test yes this is a simple tes".split("\\s+");
		System.out.println("this is:\t"+indexOf(words, "this", "is", 0));
		System.out.println("is just:\t"+indexOf(words, "is", "just", 0));
		System.out.println("this is:\t"+indexOf(words, "this", "is", 1));
	}

	private int indexOf(String[] words, String target1, String target2, int start) {
		for (int i = start; i < words.length - 1; i++) {
			if (words[i].equals(target1) && words[i + 1].equals(target2)) {
				return i + 1;
			}
		}
		return -1;
	}


	public static void main(String[] args) {
		MarkovWordTwo markov = new MarkovWordTwo();
		markov.testIndexOf();
	}

}
