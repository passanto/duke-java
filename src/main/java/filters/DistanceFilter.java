package filters;

import earthquake.Filter;
import earthquake.Location;
import earthquake.QuakeEntry;

public class DistanceFilter implements Filter {

	private Location loc;
	private double max;
	private String name;

	public DistanceFilter(Location loc, double max) {
		super();
		this.loc = loc;
		this.max = max;
	}

	public DistanceFilter(Location loc, double max, String name) {
		super();
		this.loc = loc;
		this.max = max;
		this.name = name;
	}

	@Override
	public boolean satisfies(QuakeEntry qe) {
		double d = qe.getLocation().distanceTo(loc);
		return d < max;
	}

	@Override
	public String getName() {
		return name;
	}

}
