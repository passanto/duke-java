package filters;

import earthquake.Filter;
import earthquake.QuakeEntry;

public class MagnitudeFilter implements Filter {

	private double min;
	private double max;
	private String name;

	public MagnitudeFilter(double min, double max) {
		super();
		this.min = min;
		this.max = max;
	}

	public MagnitudeFilter(double min, double max, String name) {
		super();
		this.min = min;
		this.max = max;
		this.name = name;
	}

	@Override
	public boolean satisfies(QuakeEntry qe) {
		double m = qe.getMagnitude();
		return m >= min && m <= max;
	}

	@Override
	public String getName() {
		return name;
	}

}