package filters;

import earthquake.Filter;
import earthquake.QuakeEntry;

public class DepthFilter implements Filter {

	private double min;
	private double max;
	private String name;

	public DepthFilter(double max, double min) {
		super();
		this.min = min;
		this.max = max;
	}

	public DepthFilter(double max, double min, String name) {
		super();
		this.min = min;
		this.max = max;
		this.name = name;
	}

	@Override
	public boolean satisfies(QuakeEntry qe) {
		double d = qe.getDepth();
		return d <= min && d >= max;
	}

	@Override
	public String getName() {
		return name;
	}

}
