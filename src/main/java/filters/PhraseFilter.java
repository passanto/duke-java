package filters;

import earthquake.Filter;
import earthquake.QuakeEntry;

public class PhraseFilter implements Filter {

	private String s;
	private String where;
	private String name;

	public PhraseFilter(String where, String s) {
		super();
		this.s = s;
		this.where = where;
	}

	public PhraseFilter(String where, String s, String name) {
		super();
		this.s = s;
		this.where = where;
		this.name = name;
	}

	@Override
	public boolean satisfies(QuakeEntry qe) {
		String i = qe.getInfo();
		switch (where) {
		case "start":
			return i.startsWith(s);
		case "end":
			return i.endsWith(s);
		case "any":
			return i.contains(s);
		default:
			return false;
		}
	}

	@Override
	public String getName() {
		return name;
	}

}
