package stringsFirstAssignments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Part2 {

	public static final String START_CODON = "ATG";
	public static final String END_CODON_1 = "TAA";
	public static final String END_CODON_2 = "TAG";
	public static final String END_CODON_3 = "TGA";
	public static final List<String> STOP_CODONS = new ArrayList<>(
			Arrays.asList(END_CODON_1, END_CODON_2, END_CODON_3));

	static String findSimpleGene(String dna, String startCodon, String stopCodon) {
		String res = "";
		int sPos = -1;
		int ePos = -1;
		int l = -1;
		sPos = dna.indexOf(startCodon);
		while (sPos != -1) {
			int cursor = sPos + 3;
			ePos = dna.indexOf(stopCodon, cursor);
			while (ePos != -1) {
				l = ePos - sPos + 3;
				if (l % 3 == 0) {
					System.out.println(dna.substring(sPos, ePos + 3));
					break;
				}
				cursor++;
			}
			sPos = dna.indexOf(startCodon, ePos + 3);
		}
		return res;
	}

	public static void main(String[] args) {
		findSimpleGene("AAATGCCCTAACTAGATTAAGAAACC".toUpperCase(), START_CODON, END_CODON_1);
		System.out.println("#of genes: " + findMultipleGenes("AATGCTAACTAGCTGACTAAT".toUpperCase(), START_CODON));
	}

	private static int findMultipleGenes(String dna, String startCodon) {
		int genes = 0;
		int sPos = -1;
		int ePos = -1;
		int l = -1;
		sPos = dna.indexOf(startCodon);
		while (sPos != -1) {
			int currLowestCodon = dna.length() - 1;
			int cursor = sPos + 3;
			for (int i = 0; i < STOP_CODONS.size(); i++) {
				ePos = dna.indexOf(STOP_CODONS.get(i), cursor);
				if (ePos != -1) {
					l = ePos - sPos + 3;
					if (l % 3 == 0) {
						if (ePos < currLowestCodon) {
							currLowestCodon = ePos;
						}
					}
				}
			} // for 3 loop
			if (currLowestCodon < dna.length() - 1) {
				System.out.println(" " + dna.substring(sPos, currLowestCodon + 3));
				sPos = dna.indexOf(startCodon, currLowestCodon + 3);
				genes++;
			} else {
				sPos = -1;
			}
		}
		return genes;

	}

}
