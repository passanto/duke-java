package stringsFirstAssignments;

public class Debug1 {

	public static void findAbc(String input) {
		int index = input.indexOf("abc");
		while (true) {
			if (index == -1) {
				break;
			}
			System.out.println("index+1:" + (index + 1));
			System.out.println("index+4:" + (index + 4));
			if (index > input.length() - 3) {
				break;
			}
			String found = input.substring(index + 1, index + 4);
			System.out.println(found);
			index = input.indexOf("abc", index + 4);
		}
	}

	public static void test() {
		 findAbc("abcdabc");
//		findAbc("abcdabc");// abcdabc
	}

	private static void findAbc1(String input) {
		int index = input.indexOf("abc");
		while (true) {
			if (index == -1) {
				break;
			}
			String found = input.substring(index + 1, index + 4);
			System.out.println(found);
			index = input.indexOf("abc", index + 4);
		}
	}

	public static void main(String[] args) {
		test();
	}

}
