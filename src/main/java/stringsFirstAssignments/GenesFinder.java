package stringsFirstAssignments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GenesFinder {

	// static String filePath = "src/main/resources/brca1line.fa";
	static String filePath = "src/main/resources/GRch38dnapart.fa";

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filePath));

		String line = scanner.nextLine();
		printAllGenese(line.toUpperCase());
		scanner.close();
	}

	public static void printAllGenese(String dna) {
		int startIndex = 0;
		int geneS = 0;
		int longestGene = 0;
		int cgR = 0;
		int genes60 = 0;

		while (true) {
			String currentGene = findGene(dna, startIndex);
			if (currentGene.isEmpty()) {
				break;
			}
			if (currentGene.length() > 60) {
				genes60++;
			}
			if (currentGene.length() > longestGene) {
				longestGene = currentGene.length();
			}
			if (findCGRatio(currentGene)) {
				cgR++;
			}
			startIndex = dna.indexOf(currentGene, startIndex) + currentGene.length();
			geneS++;
			// System.out.println(currentGene);
		}
		System.out.println("GeneS:" + geneS);
		System.out.println("Genes60:" + genes60);
		System.out.println("Longest:" + longestGene);
		System.out.println("cgR:" + cgR);
		countCTGCodon(dna);
	}

	public static int countCTGCodon(String dna) {
		dna = dna.toLowerCase();
		int count = dna.length() - dna.replace("ctg", "").length();
		System.out.println("CTG Count: " + count / 3);
		return count;
	}

	public static String findGene(String dna, int where) {
		int startIndex = dna.toUpperCase().indexOf("ATG", where);
		if (startIndex == -1) {
			return "";
		}
		int taaIndex = findStopCodon(dna, startIndex, "TAA");
		int tagIndex = findStopCodon(dna, startIndex, "TAG");
		int tgaIndex = findStopCodon(dna, startIndex, "TGA");

		int minIndex = 0;
		if (taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
			minIndex = tgaIndex;
		} else {
			minIndex = taaIndex;
		}
		if (minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
			minIndex = tagIndex;
		}
		if (minIndex == -1) {
			return "";
		}

		if (minIndex + 3 < dna.length()) {
			return dna.substring(startIndex, minIndex + 3);
		} else {
			return "";
		}

	}

	public static int findStopCodon(String dnaStr, int startIndex, String stopCodon) {
		int currIndex = dnaStr.toUpperCase().indexOf(stopCodon, startIndex + 3);
		while (currIndex != -1) {
			int diff = currIndex - startIndex;
			if (diff % 3 == 0) {
				return currIndex;
			} else {
				currIndex = dnaStr.toUpperCase().indexOf(stopCodon, currIndex + 1);
			}
		}
		return dnaStr.length();
	}

	private static boolean findCGRatio(String g) {
		float len = g.length();
		long countC = 0;
		countC = g.chars().filter(ch -> ch == 'C').count();
		long countG = 0;
		countG = g.chars().filter(ch -> ch == 'G').count();
		if ((double) ((countC + countG) / len) > 0.35) {
			return true;
		}
		return false;
	}

}
