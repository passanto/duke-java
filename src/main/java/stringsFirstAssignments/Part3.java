package stringsFirstAssignments;

public class Part3 {

	public static boolean twoOccurrences(String stringa, String stringb) {
		if (stringb.indexOf(stringa) != -1) {
			int idx = stringb.indexOf(stringa);
			if (stringb.indexOf(stringa, idx + stringa.length()) != -1) {
				return true;
			}
		}
		return false;
	}

	public static String lastPart(String stringa, String stringb) {
		int idx = stringb.indexOf(stringa);
		if (idx != -1) {
			return stringb.substring(idx + stringa.length(), stringb.length());
		}
		return stringb;
	}

	public static void main(String[] args) {
		String a = "by";
		String b = "A story by Abby Long";
		System.out.println(a + " " + b + "=" + twoOccurrences(a, b));
		System.out.println(a + " " + b + "=" + lastPart(a, b));

		a = "an";
		b = "banana";
		System.out.println(a + " " + b + "=" + twoOccurrences(a, b));
		System.out.println(a + " " + b + "=" + lastPart(a, b));

		a = "atg";
		b = "ctgtatgta";
		System.out.println(a + " " + b + "=" + twoOccurrences(a, b));
		System.out.println(a + " " + b + "=" + lastPart(a, b));

		a = "zoo";
		b = "forrest";
		System.out.println(a + " " + b + "=" + twoOccurrences(a, b));
		System.out.println(a + " " + b + "=" + lastPart(a, b));
	}
}
