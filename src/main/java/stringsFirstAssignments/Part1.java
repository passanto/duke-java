package stringsFirstAssignments;

public class Part1 {

	public static final String START_CODON = "ATG";
	public static final String END_CODON = "TAA";

	static String findSimpleGene(String dna) {
		String res = "";
		int sPos = -1;
		int ePos = -1;
		int l = -1;
		sPos = dna.indexOf(START_CODON);
		if (sPos != -1) {
			ePos = dna.indexOf(END_CODON, sPos + 3);
			if (ePos != -1) {
				l = ePos - sPos + 3;
				if (l % 3 == 0) {
					return dna.substring(sPos, ePos + 3);
				}
			}
		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(findSimpleGene("AAAATGCAGTAACCCATGCCC"));
	}

}
