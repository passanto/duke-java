package stringsFirstAssignments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StorageResourceAssign {

	static String filePath = "src/main/resources/brca1line.fa";

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filePath));
		String line = scanner.nextLine();
		System.out.println("genes found: "+findMultipleGenes(line.toUpperCase(), Part2.START_CODON));
		scanner.close();
	}

	private static int findMultipleGenes(String dna, String startCodon) {
		int longestGene=0;
		int cg = 0;
		int genes = 0;
		int genes60 = 0;
		int sPos = -1;
		int ePos = -1;
		int l = -1;
		sPos = dna.indexOf(startCodon);
		while (sPos != -1) {
			boolean found = false;
			int currLowestCodon = dna.length() - 1;
			int cursor = sPos + 3;
			for (int i = 0; i < Part2.STOP_CODONS.size(); i++) {
				ePos = dna.indexOf(Part2.STOP_CODONS.get(i), cursor);
				if (ePos != -1) {
					l = ePos - sPos + 3;
					if (ePos < currLowestCodon) {
						currLowestCodon = ePos;
						if (l % 3 == 0) {
							found = true;
						}
					}
				}
			} // for 3 loop
			int c;
			if (found && currLowestCodon < dna.length() - 1) {
				String g = dna.substring(sPos, currLowestCodon + 3);
				System.out.println(" " + g);
				genes++;
				if (g.length() > 59) {
					genes60++;
				}
				if (findCGRatio(g)) {
					cg++;
				}
				if (g.length()> longestGene) {
					longestGene=g.length();
				}
			}
			sPos = dna.indexOf(startCodon, currLowestCodon + 3);
		}
		System.out.println("g60: " + genes60);
		System.out.println("cg:  " + cg);
		System.out.println("longest:  " + longestGene);
		countCTGCodon(dna);
		return genes;
	}

	private static boolean findCGRatio(String g) {
		float len = g.length();
		long countC = 0;
		countC = g.chars().filter(ch -> ch == 'C').count();
		long countG = 0;
		countG = g.chars().filter(ch -> ch == 'G').count();
		if ((double) ((countC + countG) / len) > 0.35) {
			return true;
		}
		return false;
	}
	
	    public static int countCTGCodon(String dna) {
        dna = dna.toLowerCase();
        int count = dna.length() - dna.replace("ctg", "").length();
        System.out.println("CTG Count: " + count / 3);
        return count;
    }


}
