package stringsFirstAssignments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.stream.Collectors;

public class Part4 {

	static String url = "http://www.dukelearntoprogram.com/course2/data/manylinks.html";
	static String YT = "YOUTUBE.COM";
	static String QUOTE = "\"";

	public static void main(String[] argv) throws IOException {
		URL urlObj = new URL(url);
		InputStream is = urlObj.openStream();
		BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
		String urlString = buffer.lines().collect(Collectors.joining(""));

		int currSecondQuotePosition;
		int currFirstQuotePosition;

		int index = urlString.toUpperCase().indexOf(YT);
		int i=0;
		while (index != -1) {
			currSecondQuotePosition = urlString.indexOf(QUOTE, index);
			currFirstQuotePosition = urlString.lastIndexOf(QUOTE, index);
			System.out.println(++i +" "+urlString.substring(currFirstQuotePosition+1, currSecondQuotePosition));
			index = urlString.toUpperCase().indexOf(YT,currSecondQuotePosition);
		}

		//
		//
		// int currSecondQuotePosition = urlString.length();
		// int currFirstQuotePosition = urlString.length() - 1;
		//
		// currSecondQuotePosition = urlString.toUpperCase().lastIndexOf(QUOTE,
		// currSecondQuotePosition);
		// currFirstQuotePosition = urlString.toUpperCase().lastIndexOf(QUOTE,
		// currSecondQuotePosition - 1);
		//
		// while ((currSecondQuotePosition > currFirstQuotePosition) &&
		// currSecondQuotePosition > 0) {
		// String res = urlString.substring(currFirstQuotePosition + 1,
		// currSecondQuotePosition);
		// if (res.toUpperCase().contains(YT)) {
		// System.out.println("YT: " + res);
		// } else {
		// // System.out.println("not YT: " + res);
		// }
		// currSecondQuotePosition = urlString.toUpperCase().lastIndexOf(QUOTE,
		// currFirstQuotePosition);
		// currFirstQuotePosition = urlString.toUpperCase().lastIndexOf(QUOTE,
		// currSecondQuotePosition - 1);
		//
		// } // while (indxOf != -1)
	}
}
