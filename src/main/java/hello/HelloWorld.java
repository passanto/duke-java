package hello;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HelloWorld {

	static String path = "/home/antonio/eclipse-workspace/JavaProgramminCourse/src/main/resources/";
	static String file ="hello_unicode.txt";

	public static void main(String[] args) throws IOException {
		//Files.lines(Paths.get(path+file)).forEach(System.out::println);
		Path p = Paths.get(path+file);
		BufferedReader br = Files.newBufferedReader(p);
		while (null!=br.readLine()){
			System.out.println(br.readLine());
		}
	}

}
