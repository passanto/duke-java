package vigenere;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import edu.duke.FileResource;

public class VigenereBreaker {

	public static final String PATH = "/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/vigenere/messages/";
	public static final String DICS_PATH = "/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/vigenere/dictionaries/";

	public static final String FILE_PATH = PATH + "secretmessage4.txt";
	public static final String DIC_PATH = DICS_PATH + "English";
	List<String> languages = Arrays.asList("Danish", "Dutch", "English", "French", "German", "Italian", "Portuguese",
			"Spanish");

	public static void main(String[] argv) {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 2; y++) {
				System.out.println(x + ", " + y);
			}
		}

		// VigenereBreaker vb = new VigenereBreaker();
		// vb.breakVigenere();
		// vb.testSlices(vb);
		// System.out.println(vb.mostCommonCharIn(vb.readDictionary(new
		// FileResource(DIC_PATH))));

	}

	public String sliceString(//
			String message, int whichSlice, int totalSlices) {
		// REPLACE WITH YOUR CODE
		StringBuilder sb = new StringBuilder();
		for (int cP = whichSlice; cP < message.length(); cP++) {
			if ((whichSlice - cP) % totalSlices == 0) {
				char c = message.charAt(cP);
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public int[] tryKeyLength(//
			String encrypted, int klength, char mostCommon) {
		int[] key = new int[klength];
		for (int i = 0; i < klength; i++) {
			String slice = sliceString(encrypted, i, klength);
			CaesarCracker cc = new CaesarCracker();
			key[i] = cc.getKey(slice);
		}

		// WRITE YOUR CODE HERE
		return key;
	}

	public void breakVigenere() {
		FileResource fr = new FileResource(FILE_PATH);
		// FileResource dicFr = new FileResource(DIC_PATH);
		HashMap<String, HashSet<String>> dictionaries = new HashMap<>();
		for (String lang : this.languages) {
			dictionaries.put(lang, readDictionary(new FileResource(DICS_PATH + lang)));
		}

		breakForAllLangs(fr.asString(), dictionaries);

		// HashSet<String> dic = readDictionary(dicFr);
		// int k = breakForLanguage(fr.asString(), dic, true);
		// int[] key = tryKeyLength(fr.asString(), k, 'e');
		// VigenereCipher vc = new VigenereCipher(key);
		// System.out.println(vc.decrypt(fr.asString()));

		// String message = fr.asString();
		// int[] key = tryKeyLength(message, 38, 'e');
		// VigenereCipher vc = new VigenereCipher(key);
		// String d = vc.decrypt(message);
		// System.out.println(d);
		// System.out.println("Valids: " + countWords(d, dic));
		//
		// for (int i = 0; i < key.length; i++) {
		// System.out.print(key[i] + ", ");
		// }
	}

	public HashSet<String> readDictionary(FileResource fr) {
		HashSet<String> dict = new HashSet<>();
		for (String string : fr.lines()) {
			dict.add(string.toLowerCase());
		}
		return dict;
	}

	public int countWords(String message, HashSet<String> dictionary) {
		int res = 0;
		for (String word : message.split("\\W")) {
			if (dictionary.contains(word.toLowerCase())) {
				res++;
			}
		}
		return res;
	}

	public int breakForLanguage(//
			String ecnrypted, HashSet<String> dictionary, boolean print) {
		int[] KEY = null;
		int biggest = 1;
		int real = 0;
		int I = 0;
		char common = mostCommonCharIn(dictionary);
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 1; i < 101; i++) {
			int[] key = tryKeyLength(ecnrypted, i, common);
			VigenereCipher vc = new VigenereCipher(key);
			String e = vc.decrypt(ecnrypted);
			real = countWords(e, dictionary);
			if (real > biggest) {
				biggest = real;
				KEY = key;
				I = i;
			}
			map.put(i, real);
		}
		//
		// for (int i : map.keySet()) {
		// if (map.get(i) > map.get(biggest)) {
		// biggest = i;
		// }
		// }
		if (print) {
			System.out.println("Key: ");
			for (int i : KEY) {
				System.out.print(i + ",");
			}
			System.out.println();
			System.out.println("Real words: " + biggest);
			System.out.println();
		}
		return I;
	}

	public char mostCommonCharIn(HashSet<String> dictionary) {
		int A = (int) 'a';
		int[] counters = new int[26];
		for (String s : dictionary) {
			for (char ch : s.toLowerCase().toCharArray()) {
				int i = (int) ch;
				if (i - A >= 0 && i - A <= 26) {
					counters[i - A]++;
				}
			}
		}
		int max = 0;
		int most = 0;
		for (int i = 0; i < counters.length; i++) {
			if (counters[i] > max) {
				max = counters[i];
				most = i;
			}
		}
		return (char) (most + A);
	}

	public void breakForAllLangs(String ecnrypted, HashMap<String, HashSet<String>> languages) {
		Map<String, Integer> keyLenForLanguage = new HashMap<>();
		for (String language : languages.keySet()) {
			HashSet<String> dictionary = languages.get(language);
			int n = breakForLanguage(ecnrypted, dictionary, false);

			keyLenForLanguage.put(language, n);
		}
		String lang = null;
		int most;
		int biggest = 0;
		int I;
		int[] KEY = null;
		for (String language : keyLenForLanguage.keySet()) {
			HashSet<String> dictionary = languages.get(language);
			int keyLen = keyLenForLanguage.get(language);
			char common = mostCommonCharIn(dictionary);
			int[] key = tryKeyLength(ecnrypted, keyLen, common);
			VigenereCipher vc = new VigenereCipher(key);
			String e = vc.decrypt(ecnrypted);
			int real = countWords(e, dictionary);
			if (real > biggest) {
				biggest = real;
				lang = language;
				KEY = key;
			}
		}
		VigenereCipher vc = new VigenereCipher(KEY);
		String e = vc.decrypt(ecnrypted);
		System.out.println(e);
		System.out.println();
		System.out.println("Language: " + lang);
		System.out.println();
		System.out.println("KEY: ");
		for (int i : KEY) {
			System.out.print(i + ",");
		}

	}

	public void testSlices(VigenereBreaker vb) {
		System.out.println(vb.sliceString("abcdefghijklm", 0, 3));
		System.out.println(vb.sliceString("abcdefghijklm", 1, 3));
		System.out.println(vb.sliceString("abcdefghijklm", 2, 3));
		System.out.println();
		System.out.println(vb.sliceString("abcdefghijklm", 0, 4));
		System.out.println(vb.sliceString("abcdefghijklm", 1, 4));
		System.out.println(vb.sliceString("abcdefghijklm", 2, 4));
		System.out.println(vb.sliceString("abcdefghijklm", 3, 4));
		System.out.println();
		System.out.println(vb.sliceString("abcdefghijklm", 0, 5));
		System.out.println(vb.sliceString("abcdefghijklm", 1, 5));
		System.out.println(vb.sliceString("abcdefghijklm", 2, 5));
		System.out.println(vb.sliceString("abcdefghijklm", 3, 5));
		System.out.println(vb.sliceString("abcdefghijklm", 4, 5));
		System.out.println();

	}

}
