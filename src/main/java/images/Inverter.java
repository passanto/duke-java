package images;

/**
 * Convert any number of images to a gray scale version by setting all color components of each pixel to the same value.
 * 
 * @author Duke Software Team 
 */
import edu.duke.*;
import java.io.*;

public class Inverter {
	// I started with the image I wanted (inImage)
	public static ImageResource invert(ImageResource inImage) {
		// I made a blank image of the same size
		ImageResource outImage = new ImageResource(inImage.getWidth(), inImage.getHeight());
		outImage.setFileName(inImage.getFileName());
		// for each pixel in outImage
		for (Pixel pixel : outImage.pixels()) {
			// look at the corresponding pixel in inImage
			Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());
			// set pixel's red to inverted
			pixel.setRed(255 - inPixel.getRed());
			// set pixel's green to inverted
			pixel.setGreen(255 - inPixel.getGreen());
			// set pixel's blue to inverted
			pixel.setBlue(255 - inPixel.getBlue());
		}
		// outImage is your answer
		return outImage;
	}

	public void selectAndInvert() {
		DirectoryResource dr = new DirectoryResource();
		for (File f : dr.selectedFiles()) {
			ImageResource inImage = new ImageResource(f);
			ImageResource gray = invert(inImage);
			gray.draw();
		}
	}

	public void testInvert() {
		ImageResource ir = new ImageResource();
		ImageResource invert = invert(ir);
		invert.draw();
	}
}
