package images;

/**
 * Make copies of all images selected within a directory (or folder).
 * 
 * @author Duke Software Team 
 */
import edu.duke.*;
import java.io.File;

public class ImageSaver {
	public static void doSave() {
		DirectoryResource dr = new DirectoryResource();
		for (File f : dr.selectedFiles()) {
			ImageResource image = new ImageResource(f);
			String fname = image.getFileName();
			String newName = "copy-" + fname;
			image.setFileName(newName);
			image.draw();
			image.save();
		}
	}

	public static void main(String[] argv) {
		// doSave();
		// doSaveGrayScale();
		doSaveInvert();
	}

	private static void doSaveGrayScale() {
		DirectoryResource dr = new DirectoryResource();
		for (File f : dr.selectedFiles()) {
			ImageResource image = new ImageResource(f);
			image = GrayScaleConverter.makeGray(image);
			String fname = image.getFileName();
			String newName = "copyGS-" + fname;
			image.setFileName(newName);
			image.draw();
			image.save();
		}
	}

	private static void doSaveInvert() {
		DirectoryResource dr = new DirectoryResource();
		for (File f : dr.selectedFiles()) {
			ImageResource image = new ImageResource(f);
			image = Inverter.invert(image);
			String fname = image.getFileName();
			String newName = "invert-" + fname;
			image.setFileName(newName);
			image.draw();
			image.save();
		}
	}
}
