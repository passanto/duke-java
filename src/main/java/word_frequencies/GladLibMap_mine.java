package word_frequencies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import edu.duke.FileResource;
import edu.duke.URLResource;

public class GladLibMap_mine {

	private Map<String, ArrayList<String>> myMap = new HashMap<>();

	private ArrayList<String> usedWords = new ArrayList<>();
	private ArrayList<String> usedCategories = new ArrayList<>();
	private int considered = 0;
	private Random myRandom;

	private static String dataSourceURL = "http://dukelearntoprogram.com/course3/data";
	// private static String dataSourceDirectory =
	// "C:\\Users\\Ospite\\eclipse-workspace\\JavaProgramminCourse\\src\\main\\resources\\strings\\data";///
	// home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/data";
	private static String dataSourceDirectory = "/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/data/";

	public static void main(String[] argv) {
		GladLibMap_mine gl = new GladLibMap_mine();
		gl.makeStory();
	}

	public GladLibMap_mine() {
		initializeFromSource(dataSourceDirectory);
		myRandom = new Random();
	}

	public GladLibMap_mine(String source) {
		initializeFromSource(source);
		myRandom = new Random();
	}

	private void initializeFromSource(String source) {
		String[] sources = new String[9];
		sources[0] = "adjective";
		sources[1] = "noun";
		sources[2] = "color";
		sources[3] = "country";
		sources[4] = "name";
		sources[5] = "animal";
		sources[6] = "timeframe";
		sources[7] = "fruit";
		sources[8] = "verb";

		for (String c : sources) {
			myMap.put(c, readIt(source + "/" + c + ".txt"));
		}

	}

	private String randomFrom(ArrayList<String> source) {
		int index = myRandom.nextInt(source.size());
		return source.get(index);
	}

	private String getSubstitute(String label) {
		if (label.equals("number")) {
			return "" + myRandom.nextInt(50) + 5;
		}
		this.considered++;
		if (!usedCategories.contains(label)) {
			usedCategories.add(label);
		}
		return randomFrom(myMap.get(label));
	}

	private String processWord(String w) {
		int first = w.indexOf("<");
		int last = w.indexOf(">", first);
		if (first == -1 || last == -1) {
			return w;
		}
		String prefix = w.substring(0, first);
		String suffix = w.substring(last + 1);
		boolean used = false;
		String sub = "";
		while (!used) {
			sub = getSubstitute(w.substring(first + 1, last));
			if (!usedWords.contains(sub)) {
				used = true;
				usedWords.add(sub);
			}
		}
		return prefix + sub + suffix;
	}

	private void printOut(String s, int lineWidth) {
		int charsWritten = 0;
		for (String w : s.split("\\s+")) {
			if (charsWritten + w.length() > lineWidth) {
				System.out.println();
				charsWritten = 0;
			}
			System.out.print(w + " ");
			charsWritten += w.length() + 1;
		}
	}

	private String fromTemplate(String source) {
		String story = "";
		if (source.startsWith("http")) {
			URLResource resource = new URLResource(source);
			for (String word : resource.words()) {
				story = story + processWord(word) + " ";
			}
		} else {
			FileResource resource = new FileResource(source);
			for (String word : resource.words()) {
				story = story + processWord(word) + " ";
			}
		}
		return story;
	}

	private ArrayList<String> readIt(String source) {
		ArrayList<String> list = new ArrayList<String>();
		if (source.startsWith("http")) {
			URLResource resource = new URLResource(source);
			for (String line : resource.lines()) {
				list.add(line);
			}
		} else {
			FileResource resource = new FileResource(source);
			for (String line : resource.lines()) {
				list.add(line);
			}
		}
		return list;
	}

	public void makeStory() {
		System.out.println("\n");
		String story = fromTemplate(dataSourceDirectory + "/madtemplate2.txt");
		printOut(story, 60);
		System.out.println("");
		System.out.println("");
		totalWordsInMap();
		System.out.println("Considered:" + totalWordsConsidered());
	}

	private void totalWordsInMap() {
		for (String c : myMap.keySet()) {
			System.out.println(c + ": ");
			for (String s : myMap.get(c)) {
				System.out.print(s + ", ");
			}
			System.out.println("");
		}
	}

	private int totalWordsConsidered() {
		int tot = 0;
		System.out.println("");
		for (String cat : usedCategories) {
			System.out.println(cat + ", ");
			tot += myMap.get(cat).size();
		}
		return tot;
	}

}
