package word_frequencies;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import edu.duke.FileResource;

public class CharactersInPlay_mine {

	private Map<String, Integer> map = new HashMap<>();

	private ArrayList<String> characters = new ArrayList<>();
	private ArrayList<Integer> charactersCounts = new ArrayList<>();

	public static void main(String[] args) {
		CharactersInPlay_mine cip = new CharactersInPlay_mine();
		cip.tester();
	}

	public void update(String person) {
		if (!characters.contains(person)) {
			characters.add(person);
			charactersCounts.add(new Integer(0));
		}
		int idx = characters.indexOf(person);
		charactersCounts.set(idx, charactersCounts.get(idx) + 1);
		map.put(person, charactersCounts.get(idx) + 1);
	}

	public void findAllCharacters() {
		FileResource fr = new FileResource(
				"/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/WIF/errors.txt");
		for (String line : fr.lines()) {
			if (!line.isEmpty()) {
				if (line.contains(".") && line.indexOf(".") + 1 != line.length()) {
					if (!line.startsWith(" ") && !line.startsWith("ACT")) {
						String name = line.substring(0, line.indexOf("."));
						if (name.equals(name.toUpperCase())) {
							update(name);
						}
					}
				}
			}
		}
	}

	private void charactersWithNumParts(int num1, int num2) {
		int count = 0;
		for (String person : characters) {
			int idx = characters.indexOf(person);
			int c = charactersCounts.get(idx);
			if (c >= num1 && c <= num2) {
				System.out.println(person + ": " + c);
				count++;
			}
		}
		System.out.println("");
		System.out.println("count:" + count);
	}

	/*
	 * testers
	 */

	private void tester() {
		int most = 0;
		findAllCharacters();
		for (String person : characters) {
			int idx = characters.indexOf(person);
			if (charactersCounts.get(idx) > charactersCounts.get(most)) {
				most = idx;
			}
			System.out.println(person + ": " + charactersCounts.get(idx));
		}
		System.out.println("MOST: " + characters.get(most) + ": " + charactersCounts.get(most));
		System.out.println("");
		charactersWithNumParts(1, 99);

		map = map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.naturalOrder())).collect(Collectors
				.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		System.out.println("");

	}
}
