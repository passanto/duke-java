package word_frequencies;

/**
 * Find out how many times each word occurs, and
 * in particular the most frequently occurring word.
 * 
 * @author Duke Software Team
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.HashMap;

import edu.duke.FileResource;

public class WordFrequenciesMap {

	private ArrayList<String> myWords;
	private ArrayList<Integer> myFreqs;

	public static void main(String[] argv) {
		WordFrequenciesMap wfm = new WordFrequenciesMap();
		wfm.countTotWords();
	}

	public void countTotWords() {
		FileResource fr = new FileResource();
		HashMap<String, Integer> map = new HashMap<>();
		int tot = 0;
		for (String w : fr.words()) {
			w = w.toLowerCase();
			if (map.keySet().contains(w)) {
				map.put(w, map.get(w) + 1);
			} else {
				map.put(w, 1);
			}
			tot++;
		}
		for (String s : map.keySet()) {
			int occ = map.get(s);
			if (occ > 500) {
				System.out.println(occ + " " + s);
			}
		}
	}

}
