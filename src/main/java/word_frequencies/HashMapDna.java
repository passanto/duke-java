package word_frequencies;

import java.util.HashMap;
import java.util.Map;

import edu.duke.FileResource;

public class HashMapDna {

	private Map<String, Integer> codonsCounters = new HashMap<>();

	public static void main(String[] args) {
		HashMapDna hmd = new HashMapDna();
		hmd.tester();
	}

	private void buildCodonMap(int start, String dna) {
		codonsCounters = new HashMap<>();
		for (int i = start; i <= dna.length() - 3; i += 3) {
			String codon = dna.substring(i, i + 3);
			int value = 0;
			if (codonsCounters.containsKey(codon)) {
				value = codonsCounters.get(codon) + 1;
			} else {
				value = 1;
			}
			codonsCounters.put(codon, value);
		}
	}

	public String getMostCommonCodon() {
		int count = 0;
		String res = "";
		for (String currCod : codonsCounters.keySet()) {
			if (codonsCounters.get(currCod) > count) {
				res = currCod;
				count = codonsCounters.get(currCod);
			}

		}
		return res;
	}

	private void printCodonCounts(int start, int end) {
		for (String currCod : codonsCounters.keySet()) {
			int occurr = codonsCounters.get(currCod);
			if (occurr >= start && occurr <= end) {
				System.out.println(currCod + ": " + occurr);
			}
		}
	}

	private void tester() {
		FileResource fr = new FileResource();
		for (String line : fr.lines()) {
			System.out.println("Line: " + line);
			for (int i = 0; i < 3; i++) {
				System.out.println("Frame: " + i);
				buildCodonMap(i, line);
				System.out.println("unique: " + codonsCounters.keySet().size());
				String common = getMostCommonCodon();
				System.out.println("most common: " + common + " size: " + codonsCounters.get(common));
				printCodonCounts(7, 50);

			}
		}

	}

}
