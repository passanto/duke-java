package word_frequencies;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import edu.duke.DirectoryResource;

public class WordsInFiles {

	// maps a word to an ArrayList of filenames
	private Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();

	public static void main(String[] args) {
		WordsInFiles wif = new WordsInFiles();
		wif.tester();
	}

	private void addWordsFromFile(File f) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(f.getAbsolutePath()), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append(" "));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String fcontent = contentBuilder.toString();

		for (String s : fcontent.split(" ")) {
			if (map.containsKey(s)) {
				if (map.get(s).contains(f.getName())) {

				} else {
					map.get(s).add(f.getName());
				}
			} else {
				ArrayList<String> aList = new ArrayList<>();
				aList.add(f.getName());
				map.put(s, aList);

			}
		}
	}

	private void buildWordFileMap() {
		map = new HashMap<String, ArrayList<String>>();
		DirectoryResource dr = new DirectoryResource();
		for (File file : dr.selectedFiles()) {
			addWordsFromFile(file);
		}
	}

	private int maxNumber() {
		int res = 0;
		for (String word : map.keySet()) {
			int size = map.get(word).size();
			if (size > res) {
				res = size;
			}
		}
		return res;
	}

	private ArrayList<String> wordsInNumFiles(int number) {
		ArrayList<String> res = new ArrayList<>();
		for (String word : map.keySet()) {
			if (map.get(word).size() == number) {
				res.add(word);
			}
		}
		return res;
	}

	private void printFilesIn(String word) {
		for (String s : map.keySet()) {
			if (s.equals(word)) {
				for (String fileName : map.get(s)) {
					System.out.println(fileName + " ");
				}
			}
		}
	}

	private void tester() {
		buildWordFileMap();
		filesContaining("tree");
		// maxNumber();
		// ArrayList<String> a = wordsInNumFiles(4);
		// System.out.println(a);
		// printFilesIn("cats");
	}

	private void filesContaining(String word) {
		for (String file : map.get(word)) {
			System.out.println(file + ", ");
		}
	}

}
