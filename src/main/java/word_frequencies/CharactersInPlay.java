package word_frequencies;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Assignment 2: Most Frequent Word
 * 
 * @version March 15, 2016
 */
import edu.duke.FileResource;

public class CharactersInPlay {

	public static final String DIR = "/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/WIF/";
	public static final String FILE = "errors.txt";
	// instance variables
	private ArrayList<String> myCharacters;
	private ArrayList<Integer> myCharacterFreqs;

	HashMap<String, Integer> map = new HashMap<>();

	public static void main(String[] args) {
		CharactersInPlay cip = new CharactersInPlay();
		cip.tester();
	}

	/**
	 * Constructor for objects of class CharactersInPlay
	 */
	public CharactersInPlay() {
		// initialise instance variables
		myCharacters = new ArrayList<String>();
		myCharacterFreqs = new ArrayList<Integer>();
	}

	public void update(String person) {
		person = person.trim();
		int index = myCharacters.indexOf(person);
		if (index == -1) {
			myCharacters.add(person);
			myCharacterFreqs.add(1);
			map.put(person, 1);
		} else {
			int freq = myCharacterFreqs.get(index);
			myCharacterFreqs.set(index, freq + 1);
			map.put(person, freq + 1);
		}
	}

	public void findAllCharacters() {
		FileResource resource = new FileResource(DIR + FILE);
		for (String line : resource.lines()) {
			int periodInLine = line.indexOf(".");
			if (periodInLine != -1) {
				String possibleName = line.substring(0, periodInLine);
				update(possibleName);
			}
		}

	}

	public void tester() {
		findAllCharacters();
		int index = findMax();
		for (int i = 0; i < myCharacters.size(); i++) {
			System.out.println(myCharacters.get(i) + " " + myCharacterFreqs.get(i));
		}
		System.out.println("Character with most speaking parts: " + myCharacters.get(index) + " "
				+ myCharacterFreqs.get(index) + "\n");
		charactersWithNumPart(10, 15);
		map = map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.naturalOrder())).collect(Collectors
				.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		System.out.println("");

	}

	public int findMax() {
		int max = myCharacterFreqs.get(0);
		int maxIndex = 0;
		for (int k = 0; k < myCharacterFreqs.size(); k++) {
			if (myCharacterFreqs.get(k) > max) {
				max = myCharacterFreqs.get(k);
				maxIndex = k;
			}
		}
		return maxIndex;
	}

	public void charactersWithNumPart(int num1, int num2) {
		for (int k = 0; k < myCharacterFreqs.size(); k++) {
			if (myCharacterFreqs.get(k) >= num1 && myCharacterFreqs.get(k) <= num2) {
				System.out.println(myCharacters.get(k) + " " + myCharacterFreqs.get(k));
			}
		}
	}
}