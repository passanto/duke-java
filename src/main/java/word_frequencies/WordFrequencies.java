package word_frequencies;

/**
 * Find out how many times each word occurs, and
 * in particular the most frequently occurring word.
 * 
 * @author Duke Software Team
 * @version 1.0
 */
import java.util.ArrayList;

import edu.duke.FileResource;

public class WordFrequencies {
	private ArrayList<String> myWords;
	private ArrayList<Integer> myFreqs;

	public static void main(String[] argv) {
		WordFrequencies wf = new WordFrequencies();
		wf.findUnique();
		// wf.printUnique();
		wf.printMost();
	}

	private void printUnique() {
		int counter = 0;
		for (String s : myWords) {
			int idx = myWords.indexOf(s);
			// if (myFreqs.get(idx) == 1) {
			counter++;
			System.out.println(s + ": " + myFreqs.get(idx));
			// }
		}
		System.out.println("counter: " + counter);
	}

	private void printMost() {
		int most = 0;
		for (String s : myWords) {
			int idx = myWords.indexOf(s);
			if (myFreqs.get(idx) > myFreqs.get(most)) {
				most = idx;
				System.out.println(s + ": " + myFreqs.get(idx));
			}
		}
		System.out.println("most: " + myWords.get(most));
	}

	public WordFrequencies() {
		myWords = new ArrayList<String>();
		myFreqs = new ArrayList<Integer>();
	}

	public void findUnique() {
		FileResource resource = new FileResource(
				"/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/errors.txt");

		for (String s : resource.words()) {
			s = s.toLowerCase();
			int index = myWords.indexOf(s);
			if (index == -1) {
				myWords.add(s);
				myFreqs.add(1);
			} else {
				int freq = myFreqs.get(index);
				myFreqs.set(index, freq + 1);
			}
		}
		System.out.println("# unique words: " + myWords.size());
	}

	public void tester() {
		findUnique();
		System.out.println("# unique words: " + myWords.size());
		for (int i = 0; i < myWords.size(); i++) {
			System.out.println(myFreqs.get(i) + " " + myWords.get(i));
		}
		int index = findMax();
		System.out.println("max word/freq: " + myWords.get(index) + " " + myFreqs.get(index));
	}

	public int findMax() {
		int max = myFreqs.get(0);
		int maxIndex = 0;
		for (int k = 0; k < myFreqs.size(); k++) {
			if (myFreqs.get(k) > max) {
				max = myFreqs.get(k);
				maxIndex = k;
			}
		}
		return maxIndex;
	}
}
