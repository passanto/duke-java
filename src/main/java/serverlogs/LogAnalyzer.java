package serverlogs;
/**
 * Write a description of class LogAnalyzer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.util.*;
import edu.duke.*;

public class LogAnalyzer {
	private ArrayList<LogEntry> records;

	public LogAnalyzer() {
		records = new ArrayList<>();
	}

	public void readFile(String filename) {
		FileResource fr = new FileResource(filename);
		for (String s : fr.lines()) {
			records.add(WebLogParser.parseEntry(s));
		}
	}

	public void printAll() {
		for (LogEntry le : records) {
			System.out.println(le);
		}
	}

	public int countUniqueIps() {
		ArrayList<String> uniqueIps = new ArrayList<>();
		for (LogEntry le : records) {
			String ip = le.getIpAddress();
			if (!uniqueIps.contains(ip)) {
				uniqueIps.add(ip);
			}
		}
		return uniqueIps.size();
	}

	public void printAllHigherThanNum(int num) {
		System.out.println("Higher than: " + num);
		for (LogEntry le : records) {
			if (le.getStatusCode() > num) {
				System.out.println(le);
			}
		}
	}

	public void uniqueIPVisitsOnDay(String someday) {
		ArrayList<LogEntry> res = new ArrayList<>();
		ArrayList<String> ips = new ArrayList<>();
		System.out.println("Date: " + someday);
		String someMonth = someday.substring(0, 3);
		String someDay = someday.substring(4, 6);
		for (LogEntry le : records) {
			String date = le.getAccessTime().toString();
			String mon = date.substring(4, 7);
			String day = date.substring(8, 10);
			if (mon.equals(someMonth) && day.equals(someDay)) {
				if (!ips.contains(le.getIpAddress())) {
					System.out.println(le);
					res.add(le);
					ips.add(le.getIpAddress());
				}
			}
		}
		System.out.println("Tot:" + res.size());
	}

	public void countUniqueIPsInRange(int low, int high) {
		ArrayList<LogEntry> res = new ArrayList<>();
		ArrayList<String> ips = new ArrayList<>();
		System.out.println("Between: " + low + " and " + high);
		for (LogEntry le : records) {
			if (le.getStatusCode() >= low && le.getStatusCode() <= high) {
				if (!ips.contains(le.getIpAddress())) {
					res.add(le);
					ips.add(le.getIpAddress());
					System.out.println(le);
				}
			}
		}
		System.out.println("Tot:" + res.size());
	}

	HashMap<String, Integer> countVisitsPerIP() {
		// maps an IP address to the number of times that IP address appears in records
		HashMap<String, Integer> res = new HashMap<>();
		for (LogEntry le : records) {
			String ip = le.getIpAddress();
			int visits = 0;
			if (res.keySet().contains(ip)) {
				visits = res.get(ip).intValue();
			}
			res.put(ip, visits + 1);
		}
		for (String ip : res.keySet()) {
			System.out.println(ip + ": " + res.get(ip));
		}
		return res;
	}

	public Integer mostNumberVisitsByIP(HashMap<String, Integer> map) {
		// maps an IP address to the number of times that IP address appears in the web
		// log file
		int res = 0;
		String ipMost = "";
		for (String ip : map.keySet()) {
			if (map.get(ip) > res) {
				res = map.get(ip);
				ipMost = ip;
			}
		}
		System.out.println();
		System.out.println("Most visit: " + ipMost + ": " + res);
		return res;
	}

	public ArrayList<String> iPsMostVisits(HashMap<String, Integer> map) {
		// maps an IP address to the number of times that IP address appears in the web
		// log file
		ArrayList<String> res = new ArrayList<String>();
		int mostVisits = 0;
		for (String ip : map.keySet()) {
			int visits = map.get(ip);
			if (visits > mostVisits) {
				res = new ArrayList<String>();
				res.add(ip);
				mostVisits = visits;
			} else if (visits == mostVisits) {
				res.add(ip);
			}
		}
		System.out.println();
		for (String ip : res) {
			System.out.print(ip + ", ");
		}
		return res;
	}

	public HashMap<String, ArrayList<String>> iPsForDays() {
		// maps days from web logs to an ArrayList of IP addresses that occurred on that
		// day
		HashMap<String, ArrayList<String>> res = new HashMap<String, ArrayList<String>>();
		for (LogEntry le : records) {
			String ip = le.getIpAddress();
			String date = le.getAccessTime().toString();
			String mon = date.substring(4, 7);
			String day = date.substring(8, 10);
			String d = mon + " " + day;
			if (res.keySet().contains(d)) {
				res.get(d).add(ip);
			} else {
				ArrayList<String> a = new ArrayList<>();
				a.add(ip);
				res.put(d, a);
			}
		}
		System.out.println();
		System.out.println("iPsForDays");
		for (String date : res.keySet()) {
			System.out.println(date + ": " + res.get(date).size());
			for (String ip : res.get(date)) {
				System.out.print(ip + ", ");
			}
			System.out.println();
		}
		return res;
	}

	public String dayWithMostIPVisits(HashMap<String, ArrayList<String>> map) {
		// maps days from web logs to an ArrayList of IP addresses that occurred on that
		// day
		String res = "";
		int most = 0;
		for (String date : map.keySet()) {
			int size = map.get(date).size();
			if (size > most) {
				most = size;
				res = date;
			}
		}
		System.out.println();
		System.out.println("dayWithMostIPVisits: " + res);
		return res;
	}

	public ArrayList<String> iPsWithMostVisitsOnDay(
			// returns an ArrayList<String> of IP addresses that had the most accesses on
			// the given day
			HashMap<String, ArrayList<String>> map, String date) {
		// int accesses = 0;
		// String mostIp = "";
		int mostIpcount = 0;
		// map an ip to the number of visit
		HashMap<String, Integer> aux = new HashMap<>();

		ArrayList<String> res = new ArrayList<>();
		if (map.containsKey(date)) {
			ArrayList<String> ips = map.get(date);
			for (String ip : ips) {
				if (aux.containsKey(ip)) {
					int ipCount = aux.get(ip) + 1;
					if (ipCount > mostIpcount) {
						mostIpcount = ipCount;
						// mostIp = ip;
					}
					aux.put(ip, ipCount);
				} else {
					aux.put(ip, 1);
					if (1 > mostIpcount) {
						mostIpcount = 1;
					}
				}
			}
			System.out.println("");
			System.out.println("iPsWithMostVisitsOnDay: ");
			for (String s : aux.keySet()) {
				if (aux.get(s) == mostIpcount) {
					res.add(s);
					System.out.println(s + ", ");
				}
			}
		}

		return res;
	}
}
