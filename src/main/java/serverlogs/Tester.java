package serverlogs;

import java.util.ArrayList;
/**
 * Write a description of class Tester here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Tester {

	private static final String LOGS_PATH = "/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/serverLogs/";

	public static void main(String[] argv) {
		Tester t = new Tester();
		t.testLogAnalyzer();
	}

	public void testLogEntry() {
		LogEntry le = new LogEntry("1.2.3.4", new Date(), "example request", 200, 500);
		System.out.println(le);
		LogEntry le2 = new LogEntry("1.2.100.4", new Date(), "example request 2", 300, 400);
		System.out.println(le2);
	}

	public void testLogAnalyzer() {
		LogAnalyzer la = new LogAnalyzer();
		la.readFile(LOGS_PATH + "weblog2_log");
		// la.printAllHigherThanNum(400);
		// la.printAll();
		// System.out.println(la.countUniqueIps());
		// la.printAllHigherThanNum(150);
		// la.uniqueIPVisitsOnDay("Sep 27");
		// la.countUniqueIPsInRange(200, 299);
		// HashMap<String, Integer> map = la.countVisitsPerIP();

		// la.mostNumberVisitsByIP(map);
		// la.iPsMostVisits(map);
		HashMap<String, ArrayList<String>> m = la.iPsForDays();
		// la.dayWithMostIPVisits(m);
		la.iPsWithMostVisitsOnDay(m, "Sep 29");
	}

}
