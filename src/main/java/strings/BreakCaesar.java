package strings;

import edu.duke.FileResource;

public class BreakCaesar {

	public static String input = "Hi, do you want a lollipop today? I own many good flavours, but banana is outstanding";
	public static String encrypted = "WZIJK CVXZFE";
	public static String firstL = "First Legion";
	public static String start = "At noon be in the conference room with your hat on for a surprise party. YELL LOUD!";

	public static void main(String[] args) {
//		FileResource fr = new FileResource(
//				"/home/user/eclipse-workspace/JavaProgramminCourse/src/main/resources/strings/mysteryTwoKeysQuiz.txt");
//		countWordLengths(fr, new int[100]);
//		String res = "";
//		for (String s : fr.lines()) {
//			if (null != s) {
//				res += s+" ";
//			}
//		}
		decryptTwoKeys("Aal uttx hm aal Qtct Fhljha pl Wbdl. Pvxvxlx!");
	}

	public static int getKey(String s) {
		return CaesarCipher.indexOfMax(CaesarCipher.countLetterOccurrences(s));
	}

	public static void decryptTwoKeys(String encrypted) {
		String half1 = halfOfString(encrypted, 0);
		String half2 = halfOfString(encrypted, 1);
		int k1 = getDecKey(half1);
		int k2 = getDecKey(half2);
		System.out.println("k1: " + k1);
		System.out.println("k2: " + k2);

		CaesarCipher caesar = new CaesarCipher(26 - k1, 26 - k2);
		System.out.println("Decrypted:" + caesar.encrypt2(encrypted));

	}

	public static int getDecKey(String e) {
		int[] freqs = CaesarCipher.countLetterOccurrences(e);
		int maxDex = CaesarCipher.indexOfMax(freqs);
		int dKey = maxDex - 4;
		if (maxDex < 4) {
			dKey = 26 - (4 - maxDex);
		}
		return dKey;
	}

	public static void countWordLengths(FileResource fr, int[] counts) {
		for (String line : fr.lines()) {
			for (String s : line.split(" ")) {
				if (s.length() > 0) {
					char[] arr = s.toCharArray();
					if (arr.length > 0) {
						if (!Character.isLetter(arr[0])) {
							s = s.substring(1, s.length());
						}
						if (s.length() > 0) {
							if (!Character.isLetter(arr[arr.length - 1])) {
								s = s.substring(0, s.length() - 1);
							}
						} else {
							continue;
						}
						int len = s.length();
						if (len > counts.length) {
							counts[counts.length]++;
						} else {
							counts[len]++;
						}
					}
				}
			}
		}
		// for (int i = 0; i < counts.length; i++) {
		// System.out.println(i + ": " + counts[i]);
		// }
		System.out.println("max: " + CaesarCipher.indexOfMax(counts));

	}
	//
	// public static int indexOfMax(int[] values) {
	// int maxIdx = 0;
	// for (int i = 0; i < values.length; i++) {
	// if (values[i] > values[maxIdx]) {
	// maxIdx = i;
	// }
	// }
	// return maxIdx;
	// }

	public static String halfOfString(String message, int start) {
		String answer = "";
		for (int k = start; k < message.length(); k += 2) {
			answer = answer + message.charAt(k);
		}
		return answer;
	}

}
