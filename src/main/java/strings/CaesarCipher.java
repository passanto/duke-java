package strings;

public class CaesarCipher {

	public static String alphab = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static String shAlp;
	// private static int key;

	static String shAlp1;
	static String shAlp2;
	// private static int key1;
	// private static int key2;

	public CaesarCipher(int key) {
		shAlp = alphab.substring(key) + alphab.substring(0, key);
		// Caesar.key = key;
	}

	public CaesarCipher(int key1, int key2) {
		shAlp1 = alphab.substring(key1) + alphab.substring(0, key1);
		shAlp2 = alphab.substring(key2) + alphab.substring(0, key2);
		// Caesar.key1 = key1;
		// Caesar.key2 = key2;
	}

	public static void main(String[] args) {
		String d = "Hfs cpwewloj loks cd Hoto kyg Cyy.";
		CaesarCipher cc = new CaesarCipher(26-14,26-24);
		System.out.println(cc.encrypt2(d));
	}

	public String encrypt(String string) {
		StringBuilder sb = new StringBuilder(string);
		for (int i = 0; i < string.length(); i++) {
			char currChar = sb.charAt(i);
			int idx = alphab.toLowerCase().indexOf(Character.toLowerCase(currChar));
			if (idx != -1) {
				char newChar = shAlp.charAt(idx);
				sb.setCharAt(i, newChar);
			}
		}
		// System.out.println(string + " with key " + key + " = " + sb.toString());
		return sb.toString();
	}

	public String encrypt2(String string) {
		StringBuilder sb = new StringBuilder(string);
		for (int i = 0; i < string.length(); i++) {
			char currChar = sb.charAt(i);
			int idx = alphab.toLowerCase().indexOf(Character.toLowerCase(currChar));
			if (idx != -1) {
				char newChar;
				if (i % 2 == 0) {
					newChar = shAlp1.charAt(idx);
				} else {
					newChar = shAlp2.charAt(idx);
				}
				sb.setCharAt(i, newChar);
			}
		}
		return sb.toString();
	}

	public static String decrypt(String e) {
		int[] freqs = countLetterOccurrences(e);
		int maxDex = indexOfMax(freqs);
		int dKey = maxDex - 4;
		if (maxDex < 4) {
			dKey = 26 - (4 - maxDex);
		}
		CaesarCipher caesar = new CaesarCipher(26 - dKey);
		return caesar.encrypt(e);
	}

	public static int[] countLetterOccurrences(String line) {
		int[] res = new int[26];
		for (int k = 0; k < line.length(); k++) {
			char ch = Character.toUpperCase(line.charAt(k));
			int dex = CaesarCipher.alphab.indexOf(ch);
			if (dex != -1) {
				res[dex]++;
			}
		}
		return res;
	}

	public static int indexOfMax(int[] values) {
		int maxIdx = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > values[maxIdx]) {
				maxIdx = i;
			}
		}
		return maxIdx;
	}

}
