package model;
/**
 * @author user
 *
 */
public class Point {
	int x;
	int y;

	public Point() {
	}

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double distance(Point p) {
		return Math.sqrt(//
				Math.pow(x - p.getX(), 2) + //
				Math.pow(y - p.getY(), 2));
	}

}