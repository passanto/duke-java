package model;
import java.util.ArrayList;
import java.util.List;

public class Shape {
	List<Point> points = new ArrayList<>();

	public Shape addPoint(Point p) {
		this.points.add(p);
		return this;
	}

	public Point getLastPoint() {
		return points.get(0);
	}

	public Point getFirstPoint() {
		return points.get(0);
	}

	public List<Point> getPoints() {
		return points;
	}
}