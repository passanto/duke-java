package recommendetion2;

import java.io.IOException;

public interface Filter {
	public boolean satisfies(String id) throws IOException;
}
