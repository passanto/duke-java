package recommendetion2;

import java.io.IOException;
import java.util.List;

public class MovieRunnerAverage {

    public static void main(String[] argv) throws IOException {
        MovieRunnerAverage mra = new MovieRunnerAverage();
        mra.printAverageRatings();
    }

    public void printAverageRatings() throws IOException {
        SecondRatings sr = new SecondRatings("ratedmoviesfull.csv", "ratings.csv");
        System.out.println("Movies#: " + sr.getMovieSize());
        System.out.println("Raters#: " + sr.getRaterSize());

        System.out.println("Average ratings: ");
        int ratersSize = 12;
        List<Rating> avgRatings = sr.getAverageRatings(ratersSize);
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(sr.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }
}
