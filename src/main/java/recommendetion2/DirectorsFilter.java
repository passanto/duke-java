package recommendetion2;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class DirectorsFilter implements Filter {

    private List<String> directors;

    public DirectorsFilter(String directors) {
        this.directors = Arrays.asList(directors.split(","));
    }

    @Override
    public boolean satisfies(String id) throws IOException {
        String mDirectors= MovieDatabase.getDirector(id);
        for (String director:mDirectors.split(",")){
            if (directors.contains(director.trim())){
                return true;
            }
        }
        return false;
    }

}
