package recommendetion2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenreFilter implements Filter {

	private String genre;

	public GenreFilter(String genre) {
		this.genre = genre;
	}
	
	@Override
	public boolean satisfies(String id) throws IOException {
		List<String> aux = Arrays.asList(MovieDatabase.getGenres(id).
				split(","));
		List<String> gens = new ArrayList<>();
		for (String s : aux){
			gens.add(s.trim());
		}
		return gens.contains(genre);
	}

}
