package recommendetion2;

/**
 * Write a description of SecondRatings here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SecondRatings {
    private ArrayList<Movie> myMovies;
    private ArrayList<Rater> myRaters;

    public SecondRatings() throws IOException {
        // default constructor
        this("ratedmovies_short.csv", "ratings_short.csv");
    }

    public SecondRatings(String moviefile, String ratingsfile) throws IOException {
        FirstRatings fr = new FirstRatings();
        ArrayList<Movie> movies = fr.loadMovies(MovieDatabase.PATH + //
                moviefile);
        myMovies = movies;
        ArrayList<Rater> raters = fr.loadRaters(MovieDatabase.PATH + //
                ratingsfile);
        myRaters = raters;
    }

    public int getMovieSize() {
        return myMovies.size();
    }

    public int getRaterSize() {
        return myRaters.size();
    }


    /*
        PRIVATE HELPER METHODS
     */
    private double getAverageByID(String id, int minimalRaters) {
        //set double to avoid round off;
        double numOfRaters = 0.0;
        double sum = 0.0;
        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                numOfRaters += 1;
            }
        }
        if (numOfRaters >= minimalRaters) {
            for (Rater rater : myRaters) {
                if (rater.hasRating(id)) {
                    sum += rater.getRating(id);
                }
            }
            double ave = sum / numOfRaters;
            return ave;
        } else {
            return 0.0;
        }
    }

    public List<Rating> getAverageRatings(int minimalRaters) {
        ArrayList<Rating> rating = new ArrayList<Rating>();
        Rating rat = new Rating("", 0.0);
        for (Movie movie : myMovies) {
            String id = movie.getID();
            if (getAverageByID(id, minimalRaters) != 0) {
                //String title = getTitle(id);
                rat = new Rating(id, getAverageByID(id, minimalRaters));
                rating.add(rat);
            }
        }

        Collections.sort(rating);
        return rating;

    }

    public double averageRatingOneMovie(String id) {
        //set double to avoid round off;
        double numOfRaters = 0.0;
        double sum = 0.0;
        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                numOfRaters += 1;
            }
        }

        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                sum += rater.getRating(id);
            }
        }
        double ave = sum / numOfRaters;
        return ave;
    }

    public String getTitle(String id) {
        String title = "OOPS! movie not found";
        for (Movie movie : myMovies) {
            if (movie.getID().equals(id)) {
                title = movie.getTitle();
                return movie.getTitle();
            }
        }
        return title;
    }

    public String getID(String title) {
        String id = "NO SUCH TITLE";
        for (Movie movie : myMovies) {
            if (movie.getTitle().equals(title)) {
                id = movie.getID();
            }
        }
        return id;
    }


}
