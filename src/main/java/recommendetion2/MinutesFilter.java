package recommendetion2;

import java.io.IOException;

public class MinutesFilter implements Filter {

    private int min;
    private int max;

    public MinutesFilter(int minutes, int max) {
        this.min = minutes;
        this.max = max;
    }

    @Override
    public boolean satisfies(String id) throws IOException {
        int thisMinutes = MovieDatabase.getMinutes(id);
        return (thisMinutes >= min && thisMinutes <= max);
    }

}
