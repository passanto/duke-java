package recommendetion2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieRunnerSimilarRatings {

	public static void main(String[] argv) throws IOException {
		MovieRunnerSimilarRatings mrwf = new MovieRunnerSimilarRatings();
		mrwf.printSimilarRatingsByYearAfterAndMinutes();
	}

	public void printAverageRatings() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		List<Rating> ar = fr.getAverageRatings(5);
		System.out.println("found " + ar.size() + " movies");
		Collections.sort(ar);
		for (Rating r : ar) {
			System.out.println(r.getValue() + "\t" + MovieDatabase.getTitle(r.getItem()));
			break;
		}
	}

	public void printAverageRatingsByYearAfterAndGenre() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		YearAfterFilter yaf = new YearAfterFilter(1990);
		GenreFilter gf = new GenreFilter("Drama");
		AllFilters af = new AllFilters();
		af.addFilter(yaf);
		af.addFilter(gf);
		List<Rating> ar = fr.getAverageRatingsByFilter(8, af);
		System.out.println("found " + ar.size() + " movies");
		Collections.sort(ar);
		for (Rating r : ar) {
			System.out.println(r.getValue() + "\t" + MovieDatabase.getYear(r.getItem()) + "\t"
					+ MovieDatabase.getTitle(r.getItem()) + "\n\t" + MovieDatabase.getGenres(r.getItem()));
			break;
		}
	}

	public void printSimilarRatings() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		String raterId = "65";
		ArrayList<Rating> ratings = fr.getSimilarRatings(raterId, 20, 5);
		for (Rating r : ratings) {
			System.out.println(MovieDatabase.getTitle(r.getItem()) + "\t" + r.getValue());
			// break;
		}
	}

	public void printSimilarRatingsByGenre() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		ArrayList<Rating> ratings = fr.getSimilarRatingsByFilter("65", 20, 5, new GenreFilter("Action"));
		for (Rating r : ratings) {
			System.out.println(MovieDatabase.getTitle(r.getItem()) + "\t" + r.getValue() + "\n"
					+ MovieDatabase.getGenres(r.getItem()));
			break;
		}
	}

	public void printSimilarRatingsByDirector() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		ArrayList<Rating> ratings = fr.getSimilarRatingsByFilter("1034", 10, 3,
				new DirectorsFilter("Clint Eastwood,Sydney Pollack,David Cronenberg,Oliver Stone"));
		for (Rating r : ratings) {
			System.out.println(MovieDatabase.getTitle(r.getItem()) + "\t" + r.getValue() + "\n"
					+ MovieDatabase.getDirector(r.getItem()));
			break;
		}
	}

	public void printSimilarRatingsByGenreAndMinutes() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		AllFilters f = new AllFilters();
		f.addFilter(new GenreFilter("Adventure"));
		f.addFilter(new MinutesFilter(100, 200));
		ArrayList<Rating> ratings = fr.getSimilarRatingsByFilter("65", 10, 5, f);
		for (Rating r : ratings) {
			System.out.println(MovieDatabase.getTitle(r.getItem()) + "\t" + MovieDatabase.getMinutes(r.getItem())
					+ "min\t" + r.getValue() + "\n" + MovieDatabase.getGenres(r.getItem()));
			break;
		}
	}

	public void printSimilarRatingsByYearAfterAndMinutes() throws IOException {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings.csv");
		System.out.println("read data for " + RaterDatabase.size() + " raters");
		MovieDatabase.initialize("ratedmoviesfull.csv");
		System.out.println("read data for " + MovieDatabase.size() + " movies");
		AllFilters f = new AllFilters();
		f.addFilter(new YearAfterFilter(2000));
		f.addFilter(new MinutesFilter(80, 100));
		ArrayList<Rating> ratings = fr.getSimilarRatingsByFilter("65", 10, 5, f);
		for (Rating r : ratings) {
			System.out.println(MovieDatabase.getTitle(r.getItem()) + "\t" + MovieDatabase.getMinutes(r.getItem())
					+ "min\t" + r.getValue() + "\t" + MovieDatabase.getYear(r.getItem()));
			break;
		}
	}

	public void test() {
		FourthRatings fr = new FourthRatings();
		RaterDatabase.initialize("ratings_test.csv");
	}

}
