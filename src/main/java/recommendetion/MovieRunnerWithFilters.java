package recommendetion;

import java.io.IOException;
import java.util.List;

public class MovieRunnerWithFilters {

    public static void main(String[] argv) throws IOException {
        MovieRunnerWithFilters mrwf = new MovieRunnerWithFilters();
        mrwf.printAverageRatingsByDirectorsAndMinutes();
    }

    public void printAverageRatings() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 35;
        List<Rating> avgRatings = tr.getAverageRatings(ratersSize);
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(md.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }

    public void printAverageRatingsByYear() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 20;
        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize,
                new YearAfterFilter(2000));
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(md.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }


    public void printAverageRatingsByGenre() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 20;
        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize, new GenreFilter("Comedy"));
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(md.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }


    public void printAverageRatingsByMinutes() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 5;
        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize,
                new MinuteFilter(105, 135));
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(md.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }


    public void printAverageRatingsByDirectors() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 4;
        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize,
                new DirectorsFilter("Clint Eastwood,Joel Coen,Martin Scorsese,Roman Polanski,Nora Ephron,Ridley Scott,Sydney Pollack"));
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(md.getTitle(rating.getItem()) + " - " + rating.getValue());
        }
    }

    public void printAverageRatingsByYearAfterAndGenre() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 8;

        AllFilters filters = new AllFilters();
        filters.addFilter(new YearAfterFilter(1990));
        filters.addFilter(new GenreFilter("Romance"));



        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize,filters);
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(
                    md.getTitle(rating.getItem()) + " - " + rating.getValue()+ " - "
                    +md.getGenres(rating.getItem()));
        }
    }


    public void printAverageRatingsByDirectorsAndMinutes() throws IOException {
        ThirdRatings tr = new ThirdRatings("ratings.csv");
        System.out.println("Raters#: " + tr.getRaterSize());

        MovieDatabase md = new MovieDatabase();
        md.initialize("ratedmoviesfull.csv");
        System.out.println("Movies#: " + md.size());


        System.out.println("Average ratings: ");
        int ratersSize = 3;

        AllFilters filters = new AllFilters();
        filters.addFilter(new DirectorsFilter("Clint Eastwood,Joel Coen,Tim Burton,Ron Howard,Nora Ephron,Sydney Pollack"));
        filters.addFilter(new MinuteFilter(90,180));

        List<Rating> avgRatings = tr.getAverageRatingsByFilter(ratersSize,filters);
        System.out.println("More than "+ratersSize+": "+avgRatings.size());

        for (Rating rating : avgRatings) {
            System.out.println(
                    md.getTitle(rating.getItem()) + " - " + rating.getValue()+ " - "
                            +md.getDirector(rating.getItem()) + " - "+md.getMinutes(rating.getItem()));
        }
    }


}
