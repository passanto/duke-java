package recommendetion;

import java.io.IOException;

public class MinuteFilter implements Filter {

    private int min;
    private int max;

    public MinuteFilter(int minutes, int max) {
        this.min = minutes;
        this.max = max;
    }

    @Override
    public boolean satisfies(String id) throws IOException {
        int thisMinutes = MovieDatabase.getMinutes(id);
        return (thisMinutes >= min && thisMinutes <= max);
    }

}
