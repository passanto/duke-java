package recommendetion;

/**
 * Write a description of SecondRatings here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.io.IOException;
import java.util.*;


public class ThirdRatings {
    //private ArrayList<Movie> myMovies;
    private ArrayList<Rater> myRaters;

    public ThirdRatings() throws IOException {
        // default constructor
        this("ratings.csv");
    }

    public ThirdRatings(String ratingsfile) throws IOException {
        FirstRatings fr = new FirstRatings();
        ArrayList<Rater> raters = fr.loadRaters(MovieDatabase.PATH + //
                ratingsfile);
        myRaters = raters;
    }

    public int getRaterSize() {
        return myRaters.size();
    }


    /*
        PRIVATE HELPER METHODS
     */
    private double getAverageByID(String id, int minimalRaters) {
        //set double to avoid round off;
        double numOfRaters = 0.0;
        double sum = 0.0;
        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                numOfRaters += 1;
            }
        }
        if (numOfRaters >= minimalRaters) {
            for (Rater rater : myRaters) {
                if (rater.hasRating(id)) {
                    sum += rater.getRating(id);
                }
            }
            double ave = sum / numOfRaters;
            return ave;
        } else {
            return 0.0;
        }
    }

    public List<Rating> getAverageRatings(int minimalRaters) throws IOException {
        ArrayList<Rating> rating = new ArrayList<Rating>();
        ArrayList<String> movies = MovieDatabase.filterBy(new TrueFilter());
        Rating rat = new Rating("", 0.0);
        for (String id : movies) {
            if (getAverageByID(id, minimalRaters) != 0) {
                //String title = getTitle(id);
                rat = new Rating(id, getAverageByID(id, minimalRaters));
                rating.add(rat);
            }
        }

        Collections.sort(rating);
        return rating;

    }

    public double averageRatingOneMovie(String id) {
        //set double to avoid round off;
        double numOfRaters = 0.0;
        double sum = 0.0;
        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                numOfRaters += 1;
            }
        }

        for (Rater rater : myRaters) {
            if (rater.hasRating(id)) {
                sum += rater.getRating(id);
            }
        }
        double ave = sum / numOfRaters;
        return ave;
    }

    public List<Rating> getAverageRatingsByFilter(int minimalRaters, Filter filterCriteria)
            throws IOException {
        ArrayList<Rating> rating = new ArrayList<Rating>();
        ArrayList<String> movies = MovieDatabase.filterBy(filterCriteria);
        Rating rat = new Rating("", 0.0);
        for (String id : movies) {
            if (getAverageByID(id, minimalRaters) != 0) {
                //String title = getTitle(id);
                rat = new Rating(id, getAverageByID(id, minimalRaters));
                rating.add(rat);
            }
        }
        Collections.sort(rating);
        return rating;
    }

}
