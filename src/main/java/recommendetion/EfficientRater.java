package recommendetion;

/**
 * Write a description of class Rater here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.util.ArrayList;
import java.util.HashMap;

public class EfficientRater implements Rater {
    private String myID;
    private HashMap<String, Rating> myRatings;

    public EfficientRater(String id) {
        myID = id;
        myRatings = new HashMap<>();
    }

    public void addRating(String item, double rating) {
        myRatings.put(item, new Rating(item, rating));
    }

    public boolean hasRating(String item) {
        return myRatings.containsKey(item);
    }

    public String getID() {
        return myID;
    }

    public double getRating(String item) {
        for (String s : myRatings.keySet()) {
            if (s.equals(item)) {
                return myRatings.get(s).getValue();
            }
        }

        return -1;
    }

    public int numRatings() {
        return myRatings.size();
    }

    public ArrayList<String> getItemsRated() {
        return new ArrayList<String>(myRatings.keySet());
    }

    public ArrayList<Rating> getMyRatings() {
        return new ArrayList<>(myRatings.values());
    }

    public String toString() {
        String result = "Rater [id=" + myID +
                " - ratings#: " + myRatings.size() +
                " - ratings=" + myRatings.values() + "]";
        return result;
    }
}
