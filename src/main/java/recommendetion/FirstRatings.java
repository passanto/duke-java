package recommendetion;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import edu.duke.FileResource;
import org.apache.commons.csv.*;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;

public class FirstRatings {


    static CSVFormat csvFileFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader();// FILE_HEADER_MAPPING);

    private int countG;
    private int countM;
    private int numOfRatingPerRaters = 0;
    private int numOfRatingPerMovie;

    public static void main(String[] argv) throws IOException {
        FirstRatings fr = new FirstRatings();
        //fr.testLoadMovies();
        fr.testLoadRaters();
    }

    public void testLoadMovies() throws IOException {
        ArrayList<Movie> movies = loadMovies(MovieDatabase.PATH + //
                "ratedmoviesfull.csv");
        HashMap<String, Integer> directorAndWork = new HashMap<String, Integer>();
        directorWork(movies, directorAndWork);
        countGenre(movies, "Comedy");
        countMinutes(movies, 150);
        int maxValue = Collections.max(directorAndWork.values());
        String maxKey = getDirectorWithMaxWork(directorAndWork, maxValue);
        System.out.println("Movies:" + movies.size());
        System.out.println("countGenre " + countG + "\n" + "countMinutes " + countM);
        System.out.println("director of most num of movies: " + maxKey);
        System.out.println("maximum number of films directed by one director : " + maxValue);
    }

    //======================TEST LOADRATERS======================//
    public void testLoadRaters() throws IOException {
        String raterId = "193";
        ArrayList<Rater> raters = loadRaters(MovieDatabase.PATH +//
                "ratings.csv");
        //raters.forEach(r -> System.out.println(r));
        Rater raterFound = raters.stream().filter(r -> r.getID().equals(raterId)).findFirst().get();
        System.out.println("Rater " + raterId + ": " + raterFound);
        Integer maxRatings = raters.stream().//
                max(Comparator.comparing(r -> r.getMyRatings().size())).//
                orElseThrow(NoSuchElementException::new).getMyRatings().size();
        System.out.println("Max ratings#: " + maxRatings);

        List<Rater> maxRaters = raters.stream().//
                filter(r -> r.getMyRatings().size() == maxRatings).//
                collect(Collectors.toList());
        System.out.println("Max raters: ");
        maxRaters.stream().forEach(System.out::println);

        String movie = "1798709";
        int movies=0;
        for (Rater r : raters){
            for (Rating ra : r.getMyRatings()){
                if (ra.getItem().equals(movie)){
                    movies++;
                }
            }
        }
        System.out.println("Movie "+ movie+" #raters: "+movies);

        List<String> moviesRated = new ArrayList<>();
        for (Rater r : raters){
            for (Rating ra : r.getMyRatings()){
                if (!moviesRated.contains(ra.getItem())){
                    moviesRated.add(ra.getItem());
                }
            }
        }
        System.out.println("#Movies rated: "+moviesRated.size());



    }


    public ArrayList<Rater> loadRaters(String filename) throws IOException {
        ArrayList<Rater> raters = new ArrayList<Rater>();
        //FileResource fr = new FileResource(filename);

        FileReader fr = new FileReader(filename);
        CSVParser csvParser = new CSVParser(fr, csvFileFormat);
        for (CSVRecord rec : csvParser) {
        //for (CSVRecord rec : fr.getCSVParser(false)) {
            //if (!rec.get(0).equals("rater_id")) {
                String item = rec.get(1);
                double value = Double.parseDouble(rec.get(2));
                String id = rec.get(0);
                if (raters.size() == 0) {
                    Rater rater = new EfficientRater(id);
                    rater.addRating(item, value);
                    raters.add(rater);
                } else {
                    List<Rater> l = new ArrayList<Rater>(raters);
                    Iterator<Rater> it = l.iterator();
                    while (it.hasNext()) {
                        Rater r = it.next();
                        if (r.getID().equals(id)) {
                            r.addRating(item, value);
                            break;
                        } else if (!it.hasNext()) {
                            Rater rater = new EfficientRater(id);
                            rater.addRating(item, value);
                            raters.add(rater);
                            break;
                        }
                    }
                }
            //}
        }
        //System.out.println(raters.size());
        return raters;
    }


    //==================== testload helper methods ===============//

    public String getDirectorWithMaxWork(HashMap<String, Integer> directorAndWork, Integer maxValue) {
        String maxKey = "";
        for (String s : directorAndWork.keySet()) {
            if (directorAndWork.get(s) == maxValue) {
                maxKey = s;
            }
        }
        return maxKey;
    }

    public void countGenre(ArrayList<Movie> movies, String genre) {
        for (Movie movie : movies) {
            //test sepcific genre.
            if (movie.getGenres().contains(genre)) {
                countG += 1;
            }
        }
    }

    public void countMinutes(ArrayList<Movie> movies, Integer minutes) {
        for (Movie movie : movies) {
            if (movie.getMinutes() > 150) {
                countM += 1;
            }
        }
    }


    public void directorWork(ArrayList<Movie> movies, HashMap<String, Integer> directorAndWork) {
        for (Movie movie : movies) {
            if (movie.getDirector().contains(",")) {
                String[] directors = movie.getDirector().split(",");
                for (String d : directors) {
                    if (!directorAndWork.containsKey(d)) {
                        directorAndWork.put(d, 1);
                    } else {
                        int num = directorAndWork.get(d);
                        num += 1;
                        directorAndWork.put(d, num);
                    }
                }
            } else {
                String director = movie.getDirector();
                if (!directorAndWork.containsKey(director)) {
                    directorAndWork.put(director, 1);
                } else {
                    int num = directorAndWork.get(director);
                    num += 1;
                    directorAndWork.put(director, num);
                }
            }
        }

    }


    public ArrayList<Movie> loadMovies(String fileName) throws IOException {
        ArrayList<Movie> res = new ArrayList<>();
        FileReader fr = new FileReader(fileName);

        CSVParser csvParser = new CSVParser(fr, csvFileFormat);
        for (CSVRecord record : csvParser) {
            String id = record.get("id");
            String title = record.get("title");
            String year = record.get("year");
            String country = record.get("country");
            String genre = record.get("genre");
            String director = record.get("director");
            Integer minutes=0;
            try{
                minutes = Integer.parseInt(record.get("minutes"));
            }catch (Exception e){

            }
            String poster = record.get("poster");
            Movie m = new Movie(id,//
                    title,//
                    year,//
                    genre,//
                    director,//
                    country,//
                    poster,//
                    minutes);
            res.add(m);
        }
        return res;
    }
}
