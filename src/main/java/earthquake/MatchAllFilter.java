package earthquake;

import java.util.ArrayList;

public class MatchAllFilter {

	private ArrayList<Filter> filters = new ArrayList<>();

	public void addFilter(Filter filter) {
		filters.add(filter);
	}

	public boolean satisfies(QuakeEntry qe) {
		for (Filter f : filters) {
			if (!f.satisfies(qe)) {
				return false;
			}
		}
		return true;
	}

	public String getName() {
		StringBuilder sb = new StringBuilder();
		filters.forEach(f -> sb.append(f.getName()+", "));
		return sb.toString();
	}
}
