package earthquake;

import java.util.Comparator;

public class TitleAndDepthComparator implements Comparator<QuakeEntry> {

	@Override
	public int compare(QuakeEntry q1, QuakeEntry q2) {
		String t1 = q1.getInfo();
		String t2 = q2.getInfo();

		int sres = t1.compareTo(t2);
		if (sres == 0) {
			Double d1 = q1.getDepth();
			Double d2 = q2.getDepth();
			if (d1 < d2) {
				return -1;
			} else if (d1 > d2) {
				return 1;
			} else {
				return 0;
			}
		}
		return sres;

	}

}
