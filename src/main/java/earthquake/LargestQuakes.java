package earthquake;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class LargestQuakes {

	public void findLargestQuakes() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source = "data/nov20quakedata.atom";
		String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		source = EarthQuakeClient.path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		// list.forEach(l -> System.out.println(l));
		// System.out.println("read data for " + list.size());
		// System.out.println("indexOfLargest: " + indexOfLargest(list));
		getLargest(list, 50);

	}

	public int indexOfLargest(ArrayList<QuakeEntry> data) {
		QuakeEntry qe = data.stream()//
				.max(//
						Comparator.comparing(QuakeEntry::getMagnitude).reversed())
				.orElseThrow(NoSuchElementException::new);
		System.out.println("Largest: " + qe);
		return data.indexOf(qe);
	}

	public ArrayList<QuakeEntry> getLargest(ArrayList<QuakeEntry> quakeData, int howMany) {
		ArrayList<QuakeEntry> qe = quakeData.stream()//
				.sorted(Comparator.comparing(QuakeEntry::getMagnitude).reversed()).//
				collect(Collectors.toCollection(ArrayList::new));
		if (qe.size() < howMany) {
			return quakeData;
		} else {
			List<QuakeEntry> resT = qe.subList(0, howMany);
			System.out.println("Largests: " + resT.size());
			ArrayList<QuakeEntry> res = new ArrayList<QuakeEntry>(resT);
			EarthQuakeClient.printList(res);
			return res;
		}

	}

	public static void main(String[] args) {
		LargestQuakes lq = new LargestQuakes();
		lq.findLargestQuakes();
	}

}
