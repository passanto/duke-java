package earthquake;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EarthQuakeClient {

	public static String path = "/home/antonio/eclipse-workspace/JavaProgramminCourse/src/main/resources/quakes/";

	public EarthQuakeClient() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<QuakeEntry> filterByPhrase(ArrayList<QuakeEntry> quakeData, String where, String phrase) {
		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		switch (where) {
		case "start":
			Stream<QuakeEntry> qe = quakeData.stream().//
					filter(q -> q.getInfo().startsWith(phrase));//
			answer.addAll(qe.collect(Collectors.toList()));
			break;
		case "end":
			qe = quakeData.stream().//
					filter(q -> q.getInfo().endsWith(phrase));
			answer.addAll(qe.collect(Collectors.toList()));
			break;
		case "any":
			qe = quakeData.stream().//
					filter(q -> q.getInfo().contains(phrase));
			answer.addAll(qe.collect(Collectors.toList()));
			break;
		default:
			break;
		}
		return answer;
	}

	public ArrayList<QuakeEntry> filterByDepth(ArrayList<QuakeEntry> quakeData, double minDepth, double maxDepth) {
		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		answer.addAll(//
				quakeData.stream().//
						filter(q -> q.getDepth() > minDepth && //
								q.getDepth() < maxDepth)
						.//
						collect(Collectors.toList()));
		return answer;
	}

	public ArrayList<QuakeEntry> filterByMagnitude(ArrayList<QuakeEntry> quakeData, double magMin) {
		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		answer.addAll(//
				quakeData.stream().//
						filter(q -> q.getMagnitude() > magMin).//
						collect(Collectors.toList()));
		return answer;
	}

	public ArrayList<QuakeEntry> filterByDistanceFrom(ArrayList<QuakeEntry> quakeData, double distMax, Location from) {

		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		answer.addAll(//
				quakeData.stream().//
						filter(//
								q -> q.getLocation().distanceTo(from) / 1000 < distMax)
						.//
						collect(Collectors.toList()));
		return answer;
	}

	public void dumpCSV(ArrayList<QuakeEntry> list) {
		System.out.println("Latitude,Longitude,Magnitude,Info");
		for (QuakeEntry qe : list) {
			System.out.printf("%4.2f,%4.2f,%4.2f,%s\n", qe.getLocation().getLatitude(), qe.getLocation().getLongitude(),
					qe.getMagnitude(), qe.getInfo());
		}

	}

	public void bigQuakes() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		String source = path + "nov20quakedatasmall.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");
		list = filterByMagnitude(list, 5.0);
		list.stream().forEach(System.out::println);
	}

	public void closeToMe() {
		EarthQuakeParser parser = new EarthQuakeParser();
		String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		source = path + "nov20quakedatasmall.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");

		// This location is Durham, NC
		// Location city = new Location(35.988, -78.907);

		// This location is Bridgeport, CA
		Location city = new Location(38.17, -118.82);

		list = filterByDistanceFrom(list, 1000, city);
		list.forEach(e -> System.out.println(//
				e.getLocation().distanceTo(city) + " " + //
						e.getInfo()));
	}

	public void createCSV() {
		EarthQuakeParser parser = new EarthQuakeParser();
		String source = "data/nov20quakedatasmall.atom";
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		dumpCSV(list);
		System.out.println("# quakes read: " + list.size());
		for (QuakeEntry qe : list) {
			System.out.println(qe);
		}
	}

	public void quakesByPhrase() {
		EarthQuakeParser parser = new EarthQuakeParser();
		String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		source = path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");
		list = filterByPhrase(list, "any", "Can");
		printList(list);
	}

	public void quakesOfDepth() {
		EarthQuakeParser parser = new EarthQuakeParser();
		String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		source = path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");
		list = filterByDepth(list, -4000.0, -2000.0);
		printList(list);
	}

	public static void printList(ArrayList<QuakeEntry> list) {
		list.forEach(System.out::println);
		System.out.println("Found " + list.size() + " quakes");
	}

	public static void main(String[] args) {
		EarthQuakeClient eqc = new EarthQuakeClient();
		eqc.quakesByPhrase();
	}

}
