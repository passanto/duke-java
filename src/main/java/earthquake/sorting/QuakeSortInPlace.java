package earthquake.sorting;

/**
 * Write a description of class QuakeSortInPlace here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import earthquake.EarthQuakeClient;

public class QuakeSortInPlace {
	public QuakeSortInPlace() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		QuakeSortInPlace qsip = new QuakeSortInPlace();
		qsip.testSort();
	}

	public int getSmallestMagnitude(ArrayList<QuakeEntry> quakes, int from) {
		int minIdx = from;
		for (int i = from + 1; i < quakes.size(); i++) {
			if (quakes.get(i).getMagnitude() < quakes.get(minIdx).getMagnitude()) {
				minIdx = i;
			}
		}
		return minIdx;
	}

	public void sortByMagnitude(ArrayList<QuakeEntry> in) {
		for (int i = 0; i < in.size(); i++) {
			int minIdx = getSmallestMagnitude(in, i);
			QuakeEntry qi = in.get(i);
			QuakeEntry qmin = in.get(minIdx);
			in.set(i, qmin);
			in.set(minIdx, qi);
		}
	}

	public void testSort() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		String source = EarthQuakeClient.path + "earthQuakeDataDec6sample2.atom";
		// String source = "data/nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);

		System.out.println("read data for " + list.size() + " quakes");
		// sortByMagnitude(list);
		 list = sortByLargestDepth(list);
		// list = sortByMagnitudeWithBubbleSort(list);
//		sortByMagnitudeWithBubbleSortWithCheck(list);
		// sortByMagnitudeWithCheck(list);

		for (QuakeEntry qe : list) {
			System.out.println(qe);
		}

	}

	public void createCSV() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source = "data/nov20quakedata.atom";
		String source = "data/nov20quakedatasmall.atom";
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		dumpCSV(list);
		System.out.println("# quakes read: " + list.size());
	}

	public void dumpCSV(ArrayList<QuakeEntry> list) {
		System.out.println("Latitude,Longitude,Magnitude,Info");
		for (QuakeEntry qe : list) {
			System.out.printf("%4.2f,%4.2f,%4.2f,%s\n", qe.getLocation().getLatitude(), qe.getLocation().getLongitude(),
					qe.getMagnitude(), qe.getInfo());
		}
	}

	public int getLargestDepth(ArrayList<QuakeEntry> in, int from) {
		QuakeEntry temp = in.subList(from, in.size()).stream().//
				max((q1, q2) -> Double.compare(q1.getDepth(), q2.getDepth())).get();
		return in.indexOf(temp);
	}

	public ArrayList<QuakeEntry> sortByLargestDepth(ArrayList<QuakeEntry> in) {
		// List<QuakeEntry> l = in.stream().sorted//
		// ((q1, q2) -> Double.compare(q2.getDepth(), q1.getDepth())).//
		// collect(Collectors.toList());

		int limit = 50;
		for (int i = 0; i < limit; i++) {
			int indDeepest = getLargestDepth(in, i);
			QuakeEntry qi = in.get(i);
			QuakeEntry qDeepest = in.get(indDeepest);
			in.set(i, qDeepest);
			in.set(indDeepest, qi);
		}

		return in;// new ArrayList<>(l);
	}

	public ArrayList<QuakeEntry> onePassBubbleSort(ArrayList<QuakeEntry> quakeData, int numSorted) {
		for (int i = 0; i < quakeData.size() - 1 - numSorted; i++) {
			QuakeEntry q1 = quakeData.get(i);
			QuakeEntry q2 = quakeData.get(i + 1);
			if (q2.getMagnitude() < q1.getMagnitude()) {
				quakeData.set(i, q2);
				quakeData.set(i + 1, q1);
			}
		}
		return quakeData;
	}

	public ArrayList<QuakeEntry> sortByMagnitudeWithBubbleSort(ArrayList<QuakeEntry> in) {
		for (int i = 0; i < in.size() - 1; i++) {
			in = onePassBubbleSort(in, i);
		}
		return in;
	}

	public boolean checkInSortedOrder(ArrayList<QuakeEntry> quakes) {
		for (int i = 0; i < quakes.size() - 1; i++) {
			if (quakes.get(i).getMagnitude() > quakes.get(i + 1).getMagnitude()) {
				return false;
			}
		}
		return true;
	}

	public void sortByMagnitudeWithBubbleSortWithCheck(ArrayList<QuakeEntry> in) {
		for (int i = 0; i < in.size(); i++) {
			if (checkInSortedOrder(in)) {
				System.out.printf("Number of passes: %d\n", i);
				break;
			}
			onePassBubbleSort(in, i);
		}
	}

	public int sortByMagnitudeWithCheck(ArrayList<QuakeEntry> in) {
		int i = 0;
		for (i = 0; i < in.size(); i++) {
			if (!checkInSortedOrder(in)) {
				int minIdx = getSmallestMagnitude(in, i);
				QuakeEntry qi = in.get(i);
				QuakeEntry qmin = in.get(minIdx);
				in.set(i, qmin);
				in.set(minIdx, qi);
			} else {
				break;
			}
		}
		System.out.println("Required " + i + " passes");
		return i;
	}

}
