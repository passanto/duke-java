package earthquake;

import java.util.ArrayList;

import filters.DepthFilter;
import filters.DistanceFilter;
import filters.MagnitudeFilter;
import filters.PhraseFilter;

public class EarthQuakeClient2 {
	public EarthQuakeClient2() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<QuakeEntry> filter(ArrayList<QuakeEntry> quakeData, Filter f) {
		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		for (QuakeEntry qe : quakeData) {
			if (f.satisfies(qe)) {
				answer.add(qe);
			}
		}

		return answer;
	}

	public ArrayList<QuakeEntry> filter(ArrayList<QuakeEntry> quakeData, MatchAllFilter f) {
		ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
		for (QuakeEntry qe : quakeData) {
			if (f.satisfies(qe)) {
				answer.add(qe);
			}
		}
		return answer;
	}

	public void quakesWithFilter() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		String source = EarthQuakeClient.path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");

		Filter f = new MagnitudeFilter(3.5, 4.5);
		list = filter(list, f);
		f = new DepthFilter(-55000.0, -20000.0);
		list = filter(list, f);
		// Location l = new Location(39.7392, -104.9903);
		// Filter f = new PhraseFilter("end", "a");
		// list = filter(list, f);
		// f = new DistanceFilter(l, 1000000);
		// list = filter(list, f);

		for (QuakeEntry qe : list) {
			System.out.println(qe);
		}
		System.out.println("Found " + list.size());
	}

	public void createCSV() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source = "../data/nov20quakedata.atom";
		String source = "data/nov20quakedata.atom";
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		dumpCSV(list);
		System.out.println("# quakes read: " + list.size());
	}

	public void dumpCSV(ArrayList<QuakeEntry> list) {
		System.out.println("Latitude,Longitude,Magnitude,Info");
		for (QuakeEntry qe : list) {
			System.out.printf("%4.2f,%4.2f,%4.2f,%s\n", qe.getLocation().getLatitude(), qe.getLocation().getLongitude(),
					qe.getMagnitude(), qe.getInfo());
		}
	}

	public void testMatchAllFilter() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		String source = EarthQuakeClient.path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");
		MatchAllFilter maf = new MatchAllFilter();
		maf.addFilter(new MagnitudeFilter(1.0, 4.0));
		maf.addFilter(new DepthFilter(-180000.0, -30000.0));
		maf.addFilter(new PhraseFilter("any", "o"));
		ArrayList<QuakeEntry> res = filter(list, maf);
		EarthQuakeClient.printList(res);
		System.out.println("Filters used are: " + maf.getName());
		System.out.println("Found " + res.size());
	}

	public void testMatchAllFilter2() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source =
		// "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		String source = EarthQuakeClient.path + "nov20quakedata.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size() + " quakes");
		MatchAllFilter maf = new MatchAllFilter();
		maf.addFilter(new MagnitudeFilter(0.0, 5.0));
		maf.addFilter(new DistanceFilter(new Location(55.7308, 9.1153), 3000000));
		maf.addFilter(new PhraseFilter("any", "e"));
		ArrayList<QuakeEntry> res = filter(list, maf);
		EarthQuakeClient.printList(res);
		System.out.println("Found " + res.size());
	}

	public static void main(String[] args) {
		EarthQuakeClient2 eqc = new EarthQuakeClient2();
		eqc.testMatchAllFilter2();
	}

}
