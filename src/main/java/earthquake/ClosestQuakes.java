package earthquake;

/**
 * Find N-closest quakes
 * 
 * @author Duke Software/Learn to Program
 * @version 1.0, November 2015
 */
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClosestQuakes {

	public static void main(String[] argv) {
		ClosestQuakes ce = new ClosestQuakes();
		ce.findClosestQuakes();
	}

	public ArrayList<QuakeEntry> getClosest(ArrayList<QuakeEntry> quakeData, Location current, int howMany) {
		ArrayList<QuakeEntry> ret = new ArrayList<QuakeEntry>();
		quakeData.sort(//
				(QuakeEntry q1, QuakeEntry q2) -> {
					Float d1 = q1.getLocation().distanceTo(current);
					Float d2 = q2.getLocation().distanceTo(current);
					return d1.compareTo(d2);
				});

		if (quakeData.size() < howMany) {
			return quakeData;
		} else {
			return quakeData.subList(0, howMany).stream().collect(Collectors.toCollection(ArrayList::new));
		}
	}

	public void findClosestQuakes() {
		EarthQuakeParser parser = new EarthQuakeParser();
		// String source = "data/nov20quakedata.atom";
		String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		source = EarthQuakeClient.path + "nov20quakedatasmall.atom";
		ArrayList<QuakeEntry> list = parser.read(source);
		System.out.println("read data for " + list.size());

		Location jakarta = new Location(-6.211, 106.845);

		ArrayList<QuakeEntry> close = getClosest(list, jakarta, 3);
		for (int k = 0; k < close.size(); k++) {
			QuakeEntry entry = close.get(k);
			double distanceInMeters = jakarta.distanceTo(entry.getLocation());
			System.out.printf("%4.2f\t %s\n", distanceInMeters / 1000, entry);
		}
		System.out.println("number found: " + close.size());
	}

}
