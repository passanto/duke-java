package earthquake;

import java.util.Comparator;

public class TitleLastAndMagnitudeComparator implements Comparator<QuakeEntry> {

	@Override
	public int compare(QuakeEntry q1, QuakeEntry q2) {
		String t1 = q1.getInfo().substring(q1.getInfo().lastIndexOf(" ") + 1);
		String t2 = q2.getInfo().substring(q2.getInfo().lastIndexOf(" ") + 1);

		int sres = t1.compareTo(t2);
		if (sres == 0) {
			Double m1 = q1.getMagnitude();
			Double m2 = q2.getMagnitude();
			if (m1 < m2) {
				return -1;
			} else if (m1 > m2) {
				return 1;
			} else {
				return 0;
			}
		}
		return sres;

	}

}
