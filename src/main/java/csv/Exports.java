package csv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class Exports {

	static String filePath = "src/main/resources/exports/exportdata.csv";
	private static final String[] FILE_HEADER_MAPPING = { "Country", "Exports", "Value (dollars)" };

	static CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);

	public static void main(String[] args) throws IOException {
		FileReader fr = new FileReader(filePath);
		CSVParser csvParser = new CSVParser(fr, csvFileFormat);
		Long l = 999999999999L;
		 listExporterValue(csvParser, l);
//		 listExportersProduct(csvParser, "cocoa");
//		listExportersTwoProducts(csvParser, "cotton", "flowers");
	}

	private static void listExporterValue(CSVParser parser, Long l) {
		for (CSVRecord record : parser) {
			String valueString = record.get("Value (dollars)");
			try {
				long value = Long.valueOf(valueString.substring(1).replace(",", ""));
				if (value > l) {
					System.out.println(record.get("Country") + ": " + value);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	private static void listExportersProduct(CSVParser parser, String exportItem1) {
		int i = 0;
		for (CSVRecord record : parser) {
			String exports = record.get("Exports");
			if (exports.contains(exportItem1)) {
				i++;
				String valueString = record.get("Value (dollars)");
				long value = Long.valueOf(valueString.substring(1).replace(",", ""));
				System.out.println(record.get("Country") + ": " + value);
			}
		}
		System.out.println("Tot: " + i);
	}

	public static void listExportersTwoProducts(CSVParser parser, String exportItem1, String exportItem2) {
		for (CSVRecord record : parser) {
			String exports = record.get("Exports");
			if (exports.contains(exportItem1) && exports.contains(exportItem2)) {
				String valueString = record.get("Value (dollars)");
				long value = Long.valueOf(valueString.substring(1).replace(",", ""));
				System.out.println(record.get("Country") + ": " + value);
			}
		}
	}
}
