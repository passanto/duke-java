package csv;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class Weather {

	static String folderPath = "src/main/resources/nc_weather/";
	static String year = "2013/";
	static String filePath = folderPath + year + "weather-2013-09-02.csv";

	static List<String> days = Arrays.asList("weather-2014-01-19", "weather-2014-01-20");

	private static final String[] FILE_HEADER_MAPPING = { "TimeEST", "TemperatureF", "Dew PointF", "Humidity",
			"Sea Level PressureIn", "VisibilityMPH", "Wind Direction", "Wind SpeedMPH", "Gust SpeedMPH",
			"PrecipitationIn", "Events", "Conditions", "WindDirDegrees", "DateUTC" };

	static CSVFormat csvFileFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader();// FILE_HEADER_MAPPING);

	public static void main(String[] args) throws IOException {
		FileReader fr = new FileReader(filePath);
		CSVParser csvParser = new CSVParser(fr, csvFileFormat);

		// coldetHourInFile(csvParser);
		 fileWithColdestTemperature();
//		 lowestHumidityInFile(csvParser);
//		lowestHumidityInManyFiles();
//		 averageTempInFile(csvParser);
//		 averageTempHighHumInFile(csvParser, 80);
	}

	private static void fileWithColdestTemperature() throws IOException {
		File folder = new File(folderPath + year);
		File[] days = folder.listFiles();

		CSVRecord minRecord = null;
		Double min = Double.MAX_VALUE;
		FileReader minFileReader = null;
		String minFile = null;
		for (File dayFile : days) {
			String day = dayFile.getAbsolutePath();
			// String file = folderPath + year + day + ".csv";
			FileReader fr = new FileReader(day);
			CSVParser csvParser = new CSVParser(fr, csvFileFormat);
			for (CSVRecord record : csvParser) {
				String t = record.get("TemperatureF");
				if (!t.equals("-9999")) {
					Double temp = Double.parseDouble(t);
					if (temp < min) {
						min = temp;
						minRecord = record;
						minFileReader = fr;
						minFile = day;
					}
				}
			}
			csvParser.close();
		}
		System.out.println("Coolest day: " + minFile);
		System.out.println("Coolest temp: " + min);
		System.out.println("All temp on that day: ");
		String file = minFile;
		FileReader fr = new FileReader(file);
		CSVParser csvParser = new CSVParser(fr, csvFileFormat);
		for (CSVRecord record : csvParser) {
			String time = record.get("TimeEST");
			String t = record.get("TemperatureF");
			if (!t.equals("-9999")) {
				System.out.print(time + " ");
				System.out.println(t);
			}
		}
		csvParser.close();

	}

	public static void coldetHourInFile(CSVParser parser) {
		CSVRecord minRecord = null;
		double min = Double.MAX_VALUE;
		for (CSVRecord record : parser) {
			String t = record.get("TemperatureF");
			if (!t.equals("-9999")) {
				try {
					Double temp = Double.parseDouble(t);
					if (temp < min) {
						min = temp;
						minRecord = record;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		if (minRecord != null && min < Double.MAX_VALUE) {
			System.out.println("coolest: " + minRecord.get("DateUTC") + ": " + min);

		}
	}

	public static CSVRecord lowestHumidityInFile(CSVParser parser) {
		CSVRecord cr = null;
		double min = Double.MAX_VALUE;
		for (CSVRecord record : parser) {
			String t = record.get("Humidity");
			try {
				Double hum = Double.parseDouble(t);
				if (hum < min) {
					min = hum;
					cr = record;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (cr != null && min < Double.MAX_VALUE) {
			System.out.println("lowest humidity: " + cr.get("DateUTC") + ": " + min);
		}
		return cr;
	}

	public static CSVRecord lowestHumidityInManyFiles() throws IOException {
		File folder = new File(folderPath + year);
		File[] days = folder.listFiles();

		CSVRecord minRecord = null;
		Double min = Double.MAX_VALUE;
		String minDate = null;
		for (File dayFile : days) {
			String day = dayFile.getAbsolutePath();
			FileReader fr = new FileReader(day);
			CSVParser csvParser = new CSVParser(fr, csvFileFormat);
			for (CSVRecord record : csvParser) {
				String t = record.get("Humidity");
				try {
					Double hum = Double.parseDouble(t);
					if (hum < min) {
						min = hum;
						minRecord = record;
						minDate = record.get("DateUTC");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			csvParser.close();
		}
		System.out.println("Driest day: " + minDate);
		System.out.println("Driest hum: " + min);
		return minRecord;
	}

	public static double averageTempInFile(CSVParser parser) {
		double d = 0;
		double totTemp = 0;
		int days = 0;
		for (CSVRecord record : parser) {
			String t = record.get("TemperatureF");
			try {
				Double temp = Double.parseDouble(t);
				days++;
				totTemp += temp;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		d = totTemp / days;
		System.out.println("Average: " + d);
		return d;
	}

	public static CSVRecord averageTempHighHumInFile(CSVParser parser, int minHum) throws IOException {
		CSVRecord minRecord = null;
		double d = 0;
		double totTemp = 0;
		int days = 0;

		for (CSVRecord record : parser) {
			String h = record.get("Humidity");
			String t = record.get("TemperatureF");
			try {
				Double hum = Double.parseDouble(h);
				Double temp = Double.parseDouble(t);
				if (hum > minHum) {
					days++;
					totTemp += temp;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		parser.close();
		if (days != 0) {
			System.out.println("AVG hum: " + totTemp / days);
		} else {
			System.out.println("no days...");
		}
		return minRecord;
	}

}
