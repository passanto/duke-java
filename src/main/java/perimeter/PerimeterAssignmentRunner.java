package perimeter;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import model.Point;
import model.Shape;

public class PerimeterAssignmentRunner {

	static String path = "src/main/resources/perimeterData/";

	static final int dataTestsNum = 6;
	static final int examplesNum = 6;
	static final String dataFileName = "datatest";
	static final String examplesFileName = "example";

	public static Shape getshape(String fileName) throws FileNotFoundException {
		fileName = path + fileName;
		Scanner scanner = new Scanner(new File(fileName));
		Shape s = new Shape();
		Point p;
		while (scanner.hasNextLine()) {
			p = new Point();
			String[] line = scanner.nextLine().split(",");
			p.setX(Integer.parseInt(line[0].trim()));
			p.setY(Integer.parseInt(line[1].trim()));
			s.addPoint(p);
		}
		scanner.close();
		return s;
	}

	public static double getPerimeter(Shape s) {
		double totalPerim = 0.0;
		Point prevPt = s.getFirstPoint();
		Point currPt = null;
		for (int i = 1; i < s.getPoints().size(); i++) {
			currPt = s.getPoints().get(i);
			double currDist = prevPt.distance(currPt);
			totalPerim = totalPerim + currDist;
			prevPt = currPt;
		}
		totalPerim = totalPerim + s.getFirstPoint().distance(currPt);
		return totalPerim;
	}

	public int getNumPoints(Shape s) {
		return s.getPoints().size();
	}

	public static double getAverageLength(Shape s) {
		List<Point> p = s.getPoints();
		return getPerimeter(s) / p.size();
	}

	public static double getLargestSide(Shape s) {
		double r = 0;
		List<Point> p = s.getPoints();
		Point prevPt = s.getFirstPoint();
		// For each point currPt in the shape,
		Point currPt = null;
		double currDist;
		for (int i = 1; i < p.size(); i++) {
			currPt = p.get(i);
			// Find distance from prevPt point to currPt
			currDist = prevPt.distance(currPt);
			if (currDist > r) {
				r = currDist;
			}
			prevPt = currPt;
		}
		currDist = p.get(0).distance(currPt);
		if (currDist > r) {
			r = currDist;
		}
		return r;
	}

	public static double getLargestX(Shape s) {
		double d = 0;
		List<Point> points = s.getPoints();
		for (Point p : points) {
			if (p.getX() > d) {
				d = p.getX();
			}
		}
		return d;
	}

	public static double getLargestPerimeterMultipleFiles() throws FileNotFoundException {
		double biggP = 0;
		for (int i = 1; i < 7; i++) {
			Shape s = getshape("datatest" + i + ".txt");
			double cP = getPerimeter(s);
			if (cP > biggP) {
				biggP = cP;
			}
		}
		// Put code here
		return biggP;
	}

	public static String getFileWithLargestPerimeter() throws FileNotFoundException {
		double biggP = 0;
		String bigF = null;
		for (int i = 1; i < 5; i++) {
			String fileName = "example" + i + ".txt";
			Shape s = getshape(fileName);
			double cP = getPerimeter(s);
			System.out.println("Perimeter of shape " + fileName + ": " + cP);
			if (cP > biggP) {
				biggP = cP;
				bigF = (fileName);
			}
		}
		System.out.println("Biggest Perimeter: " + biggP + "  FILE: " + bigF);
		// Put code here
		return bigF;
	}

	public static void testPerimeter() throws FileNotFoundException {
		String f = "example1.txt";
		Shape s = getshape(f);
		double length = getPerimeter(s);
		System.out.println("perimeter  " + f + " = " + length);
		System.out.println("getLargestX = " + getLargestX(s));
	}

	public void testPerimeterMultipleFiles() {
		// Put code here
	}

	public void testFileWithLargestPerimeter() {
		// Put code here
	}

	// This method creates a triangle that you can use to test your other methods
	public void triangle() {
		Shape triangle = new Shape();
		triangle.addPoint(new Point(0, 0));
		triangle.addPoint(new Point(6, 0));
		triangle.addPoint(new Point(3, 6));
		for (Point p : triangle.getPoints()) {
			System.out.println(p);
		}
		double peri = getPerimeter(triangle);
		System.out.println("perimeter = " + peri);
	}

	// This method prints names of all files in a chosen folder that you can use to
	// // test your other methods
	// public void printFileNames() {
	// DirectoryResource dr = new DirectoryResource();
	// for (File f : dr.selectedFiles()) {
	// System.out.println(f);
	// }
	// }

	public static void main(String[] args) throws FileNotFoundException, Exception {
		System.out.println(getFileWithLargestPerimeter());//(getshape("datatest4.txt")));
	}
}
